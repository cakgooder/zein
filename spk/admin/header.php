<nav class="navbar navbar-default top-navbar" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Beranda</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php?page=index"><strong>ADMIN PANEL</strong></a>
    </div>

    <ul class="nav navbar-top-links navbar-right">
        <!--                <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                            </a>
                            
                             /.dropdown-messages 
                        </li>-->
        <!--                 /.dropdown 
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                            </a>
                            
                             /.dropdown-tasks 
                        </li>
                         /.dropdown 
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                            </a>
                            
                             /.dropdown-alerts 
                        </li>-->
        <!-- /.dropdown -->
       <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <a href="../user/p_logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
                    <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
</nav>
<!--/. NAV TOP  -->
<nav class="navbar-default navbar-side" role="navigation">
    <div id="sideNav" href="#"></div>
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">

            <li>
                <a class="active-menu" href="index.php?page=index"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>
            <!--                    <li>
                                    <a href="ui-elements.html"><i class="fa fa-desktop"></i> UI Elements</a>
                                </li>
                                <li>
                                    <a href="chart.html"><i class="fa fa-bar-chart-o"></i> Charts</a>
                                </li>
                                <li>
                                    <a href="tab-panel.html"><i class="fa fa-qrcode"></i> Tabs & Panels</a>
                                </li>
                                
                                <li>
                                    <a href="table.html"><i class="fa fa-table"></i> Responsive Tables</a>
                                </li>
                                <li>
                                    <a href="form.html"><i class="fa fa-edit"></i> Forms </a>
                                </li>-->

            <li>
                <a href="#"><i class="fa fa-sitemap"></i> Data Admin<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="index.php?page=data_admin">Lihat Data</a>
                    </li>
                    <li>
                        <a href="index.php?page=input_admin">Insert Data Baru</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap"></i> Data Raket<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="index.php?page=data_produsen">Lihat Data</a>
                    </li>
                    <li>
                        <a href="index.php?page=input">Insert Data Baru</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap"></i> Data Tipe Raket<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="index.php?page=data_tipe">Lihat Data</a>
                    </li>
                    <li>
                        <a href="index.php?page=input_tipe">Insert Data Baru</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap"></i> Data Kriteria<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="index.php?page=data_kriteria">Lihat Data</a>
                    </li>
                    <li>
                        <a href="index.php?page=input_kriteria">Insert Data Baru</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap"></i> Data Subkriteria<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="index.php?page=data_subkriteria">Lihat Data </a>
                    </li>
                    <li>
                        <a href="index.php?page=input_subkriteria">Insert Data Baru</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap"></i> Data Test<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="index.php?page=data_test">Lihat Data</a>
                    </li>
                    <li>
                        <a href="index.php?page=input_test">Insert Data Baru</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap"></i> Data Responden<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="index.php?page=data_responden">Lihat Data</a>
                    </li>
                </ul>
            </li>
            <!--                    <li>
                                    <a href="empty.html"><i class="fa fa-fw fa-file"></i> Empty Page</a>
                                </li>-->
        </ul>

    </div>

</nav>
<!-- /. NAV SIDE  -->