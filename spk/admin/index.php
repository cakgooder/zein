﻿<!DOCTYPE html>
<?php
session_start();
if (empty($_SESSION['username'])) {
    header('Location:../user/index.php?page=login');
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">


    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:50:59 GMT -->
    <head>
        <title>Administator</title>
    </head>

    <body>
        <div id="wrapper">
            <?php include("header.php"); ?>

            <?php include ("content/content.php"); ?>

            <!-- /. PAGE WRAPPER  -->
        </div>


    </body>


    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:06 GMT -->
</html>