﻿<!DOCTYPE html>
<?php
session_start();
if (empty($_SESSION['username'])) {
    header('Location: ../user/index.php?page=login');
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/form.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:46 GMT -->
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link href="assets/css/bootstrap.css" rel="stylesheet" />
       <!--  FontAwesome Styles -->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- Custom Styles -->
        <link href="assets/css/custom-styles.css" rel="stylesheet" />
      <!--   Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    </head>
    <body>
        <div id="wrapper">

            <div id="page-wrapper" >
                <div class="header"> 
                    <h1 class="page-header">
                        Form  <small>Update</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Beranda</a></li>
                        <li><a href="#">Update</a></li>
                        <li class="active">Data Produsen</li>
                    </ol> 

                </div>

                <div id="page-inner"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Form Update Produsen Raket
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?php
                                            include("./content/connect.php");
                                            $sql = mysql_query("select * from produsen_raket where ID_RAKET='" . $_GET['id'] . "'");
                                            $row = mysql_fetch_array($sql);
                                            ?>
                                            <form role="form" method="POST" action="content/raket/p_produsen/update_dataprodusen.php">

                                                <div class="form-group has-warning">
                                                    <label>ID RAKET</label>
                                                    <input type="hidden" name="id_raket" value="<?php echo $row['ID_RAKET']; ?>" class="form-control" placeholder="isi disini" id="inputWarning">
                                                </div>
                                                <div class="form-group">
                                                    <label>NAMA PRODUSEN RAKET</label>
                                                    <input name="nama_produsenraket" value="<?php echo $row['NAMA_PRODUSENRAKET']; ?>" class="form-control" placeholder="isi disini">
                                                </div>

                                                <button type="submit" class="btn btn-default">Update</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <?php include ("footer.php"); ?>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->

<!--        /. WRAPPER  
        JS Scripts
        jQuery Js -->
        <script src="assets/js/jquery-1.10.2.js"></script>
<!--        Bootstrap Js -->
        <script src="assets/js/bootstrap.min.js"></script>
<!--        Metis Menu Js -->
        <script src="assets/js/jquery.metisMenu.js"></script>
<!--        Custom Js -->
        <script src="assets/js/custom-scripts.js"></script>


    </body>

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/form.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:46 GMT -->
</html>
