<!DOCTYPE html>
<?php 
            session_start();
                if (empty($_SESSION['username'])) {
                    header('Location:../user/index.php?page=login');
                }
        ?>
<html xmlns="http://www.w3.org/1999/xhtml">


<!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:36 GMT -->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ADMINISTRATOR</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="assets/js/Lightweight-Chart/cssCharts.css"> 
</head>

<body>
    <div id="wrapper">
      
        <div id="page-wrapper">
                <div class="header"> 
                    <h1 class="page-header">
                        Beranda <small>Halaman Admin</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Beranda</a></li>
                        <li class="active">Admin</li>
                    </ol> 

                </div>
                <div id="page-inner">

                    <!-- /. ROW  -->

                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="panel panel-primary text-center no-boder blue">
                                <div class="panel-left pull-left blue">
                                    <i class="fa fa-eye fa-5x"></i>

                                </div>
                                <div class="panel-right">
                                    <h3>10,253</h3>
                                    <strong> Daily Visits</strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="panel panel-primary text-center no-boder blue">
                                <div class="panel-left pull-left blue">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>

                                <div class="panel-right">
                                    <h3>33,180 </h3>
                                    <strong> Sales</strong>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="panel panel-primary text-center no-boder blue">
                                <div class="panel-left pull-left blue">
                                    <i class="fa fa fa-comments fa-5x"></i>

                                </div>
                                <div class="panel-right">
                                    <h3>16,022 </h3>
                                    <strong> Comments </strong>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="panel panel-primary text-center no-boder blue">
                                <div class="panel-left pull-left blue">
                                    <i class="fa fa-users fa-5x"></i>

                                </div>
                                <div class="panel-right">
                                    <h3>36,752 </h3>
                                    <strong>No. of Visits</strong>

                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Tabel Penjelasan Singkat
                                </div>
                                <div class="panel-body">
                                    <ul class="nav nav-pills">
                                        <li class=""><a href="#home-pills" data-toggle="tab">Beranda</a>
                                        </li>
                                        <li class=""><a href="#profile-pills" data-toggle="tab">Sidebar Tab</a>
                                        </li>
                                        <li class=""><a href="#messages-pills" data-toggle="tab">Data Tab</a>
                                        </li>
                                        <li class="active"><a href="#settings-pills" data-toggle="tab">Top Tab</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div class="tab-pane fade" id="home-pills">
                                            <h4>Beranda</h4>
                                            <p>Pada Halaman ini merupakan halaman awal daripada halaman administrator untuk melakukan pengolahan data.</p>
                                        </div>
                                        <div class="tab-pane fade" id="profile-pills">
                                            <h4>Sidebar Tab</h4>Pada panel tab ini berisi proses pengolahan data master yakni Data Produsen Raket, Data Tipe Raket, Data Kriteria, Data Subkriteria, dan Data Test untuk poses perhitungan hasil rekomendasi dengan metode FAHP.</p>
                                        </div>
                                        <div class="tab-pane fade" id="messages-pills">
                                            <h4>Data Tab</h4>
                                            <p>Pada panel tab ini merupakan proses menampilkan data tabel, proses insert data baru, proses edit data, dan proses delete data. </p>
                                        </div>
                                        <div class="tab-pane fade active in" id="settings-pills">
                                            <h4>Top Tab</h4>
                                            <p> Pada panel tab ini terdapat tombol button untuk proses log out administrator.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <?php include ("footer.php"); ?> 
                </div>
                     
                <!-- /. PAGE INNER  -->
            </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="assets/js/bootstrap.min.js"></script>
     
    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
    
    
    <script src="assets/js/easypiechart.js"></script>
    <script src="assets/js/easypiechart-data.js"></script>
    
     <script src="assets/js/Lightweight-Chart/jquery.chart.js"></script>
    
    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>

      <script>
    
      </script>

</body>


<!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:36 GMT -->
</html>