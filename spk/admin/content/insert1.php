﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/form.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:46 GMT -->
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Bootstrap Admin Theme : Master</title>
        <!-- Bootstrap Styles-->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FontAwesome Styles-->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- Custom Styles-->
        <link href="assets/css/custom-styles.css" rel="stylesheet" />
        <!-- Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    </head>
        <body>
        <div id="wrapper">

            <div id="page-wrapper" >
                <div class="header"> 
                    <h1 class="page-header">
                        Forms Page <small>Best form elements.</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Forms</a></li>
                        <li class="active">Data</li>
                    </ol> 

                </div>

                <div id="page-inner"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Basic Form Elements
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <form role="form">
                                                
                                                <div class="form-group">
                                                    <label>Text Input with Placeholder</label>
                                                    <input class="form-control" placeholder="Enter text">
                                                </div>
                                                
                                                <div class="form-group">
                                                    
                                                </div>
                                                <div class="form-group">
                                                    
                                                </div>
                                                <div class="form-group">
                                                   
                                                </div>
                                                <div class="form-group">
                                                    
                                                </div>
                                                <div class="form-group">
                                                   
                                                </div>
                                                <div class="form-group">
                                                    
                                                </div>
                                                <button type="submit" class="btn btn-default">Submit Button</button>
                                                <button type="reset" class="btn btn-default">Reset Button</button>
                                            </form>
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                        <div class="col-lg-6">
										
                                        </div>
                                        <!-- /.col-lg-6 (nested) -->
                                    </div>
                                    <!-- /.row (nested) -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <?php include ("../footer.php"); ?>
                </div>
                <!-- /. PAGE INNER  -->
            </div>
            <!-- /. PAGE WRAPPER  -->
        </div>
        <!-- /. WRAPPER  -->
        <!-- JS Scripts-->
        <!-- jQuery Js -->
        <script src="assets/js/jquery-1.10.2.js"></script>
        <!-- Bootstrap Js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Metis Menu Js -->
        <script src="assets/js/jquery.metisMenu.js"></script>
        <!-- Custom Js -->
        <script src="assets/js/custom-scripts.js"></script>


    </body>

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/form.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:46 GMT -->
</html>
