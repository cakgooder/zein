<!DOCTYPE html>
<?php 
            session_start();
                if (empty($_SESSION['username'])) {
                    header('Location: ../user/index.php?page=login');
                }
        ?>
<html xmlns="http://www.w3.org/1999/xhtml">

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/table.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:37 GMT -->
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Bootstrap Styles-->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FontAwesome Styles-->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- Morris Chart Styles-->
        <!-- Custom Styles-->
        <link href="assets/css/custom-styles.css" rel="stylesheet" />
        <!-- Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <!-- TABLE STYLES-->
        <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    </head>
    <body>
        <div id="wrapper">
       
            <div id="page-wrapper" >
                <div class="header"> 
                    <h1 class="page-header">
                        Tabel <small>Data Test</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Beranda</a></li>
                        <li><a href="#">Tabel</a></li>
                        <li class="active">Data Test</li>
                    </ol> 

                </div>
                <?php include("./content/connect.php"); ?>
                <div id="page-inner"> 

                    <div class="row">
                        <div class="col-md-12">
                            <!-- Advanced Tables -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Tabel Test
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Produsen</th>
                                                    <th>Nama Tipe Raket</th>
                                                    <?php $sk=mysql_query("SELECT * from kriteria order by id_kriteria");
                                                    while ($isk=mysql_fetch_array($sk)) {
                                                         ?>
                                                        <th><?php echo $isk['NAMA_KRITERIA'] ?></th>

                                                         <?php
                                                     } ?>
                                                        <th style="text-align: center">AKSI</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                
                                                $sql = mysql_query("select * from tipe_raket a, produsen_raket b where a.id_raket=b.id_raket");
                                                $no = 1;
                                                while ($row = mysql_fetch_array($sql)) {
                                                   
                                                    $ssk=mysql_query("SELECT * from data_test a, subkriteria b where a.id_subkriteria=b.id_subkriteria and a.id_tipe = '$row[ID_TIPE]'"); 
                                                        if(mysql_num_rows($ssk)>0){


                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td><?php echo $no++; ?></td>
                                                        <td class="center"><?php echo $row['NAMA_PRODUSENRAKET']; ?></td>
                                                         <td class="center"><?php echo $row['NAMA_TIPERAKET']; ?></td>
                                                        <?php while ($issk=mysql_fetch_array($ssk)) {
                                                                ?>
                                                            <td class="center"><?php echo $issk['NAMA_SUBKRITERIA']; ?></td>
                                                                <?php
                                                                $id_dt = $issk['ID_DATA'];
                                                            }
                                                        ?>
                                                        <td class="center"><a href="./?page=update_test&id=<?php echo $id_dt;?>"><button class="btn btn-primary"><i class="fa fa-edit "></i> Edit</button></a></td>
                                                        <!-- confirm('apakah yakin akan dihapus?')" class="disabled"><button class="btn btn-danger disabled "><i class="fa fa-pencil"></i> Delete</button></a></td> -->
                                                    </tr>
                                                <?php }
                                                } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <!--End Advanced Tables -->
                        </div>
                    </div>
                    <!-- /. ROW  -->
                </div>
                <?php include ("footer.php"); ?>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
        <!-- /. WRAPPER  -->
        <!-- JS Scripts-->
        <!-- jQuery Js -->
        <script src="assets/js/jquery-1.10.2.js"></script>
        <!-- Bootstrap Js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Metis Menu Js -->
       <script src="assets/js/jquery.metisMenu.js"></script>
        <!-- DATA TABLE SCRIPTS -->
       <script src="assets/js/dataTables/jquery.dataTables.js"></script>
        <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <!-- Custom Js -->
        <script src="assets/js/custom-scripts.js"></script>


    </body>

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/table.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:46 GMT -->
</html>
