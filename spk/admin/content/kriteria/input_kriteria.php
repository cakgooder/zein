<!DOCTYPE html>
<?php
session_start();
if (empty($_SESSION['username'])) {
    header('Location:../user/index.php?page=login ');
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/form.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:46 GMT -->
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<!--        
        Bootstrap Styles-->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
<!--        FontAwesome Styles-->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
<!--        Morris Chart Styles-->

<!--        Custom Styles-->
        <link href="assets/css/custom-styles.css" rel="stylesheet" />
<!--        Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<!--        TABLE STYLES-->
        <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    </head>
    <body>
        <div id="wrapper">

            <div id="page-wrapper" >
                <div class="header"> 
                    <h1 class="page-header">
                        Form  <small>Input</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Beranda</a></li>
                        <li><a href="#">Insert</a></li>
                        <li class="active">Data Kriteria</li>
                    </ol> 

                </div>

                <div id="page-inner"> 
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Form Input Kriteria
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <form role="form" method="POST" action="content/kriteria/p_kriteria/insert_datakriteria.php">

                                                <div class="form-group">
                                                    <label>ID KRITERIA</label>
                                                    <input name="id_kriteria" class="form-control" placeholder="isi disini" autofocus>
                                                </div>
                                                <div class="form-group">
                                                    <label>NAMA KRITERIA</label>
                                                    <input name="nama_kriteria" class="form-control" placeholder="isi disini">
                                                </div>

                                                <button type="submit" class="btn btn-default">Submit</button>
                                                <button type="reset" class="btn btn-default">Reset</button>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.col-lg-6 (nested) -->
                                </div>
                                <!-- /.row (nested) -->
                            </div>
                            <!-- /.panel-body -->
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <?php include ("footer.php"); ?>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!--             /. PAGE WRAPPER  -->
       
        <!--         /. WRAPPER  
                 JS Scripts
                 jQuery Js -->
        <script src="assets/js/jquery-1.10.2.js"></script>
        <!--         Bootstrap Js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!--         Metis Menu Js -->
        <script src="assets/js/jquery.metisMenu.js"></script>
        <!--         Custom Js -->
        <script src="assets/js/custom-scripts.js"></script>


    </body>

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/form.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:46 GMT -->
</html>
