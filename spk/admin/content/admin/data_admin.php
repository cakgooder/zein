﻿<!DOCTYPE html>
<?php 
            session_start();
                if (empty($_SESSION['username'])) {
                    header('Location:../user/index.php?page=login');
                }
        ?>
<html xmlns="http://www.w3.org/1999/xhtml">

    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/table.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:37 GMT -->
     <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Bootstrap Styles-->
        <link href="assets/css/bootstrap.css" rel="stylesheet" />
        <!-- FontAwesome Styles-->
        <link href="assets/css/font-awesome.css" rel="stylesheet" />
        <!-- Morris Chart Styles-->
        <!-- Custom Styles-->
        <link href="assets/css/custom-styles.css" rel="stylesheet" />
        <!-- Google Fonts-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
        <!-- TABLE STYLES-->
        <link href="assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    </head>
    <body>
        <div id="wrapper">
       
            <div id="page-wrapper" >
                <div class="header"> 
                    <h1 class="page-header">
                        Tabel <small>Data Admin</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#">Beranda</a></li>
                        <li><a href="#">Tabel</a></li>
                        <li class="active">Data Admin</li>
                    </ol> 

                </div>

                <div id="page-inner"> 

                    <div class="row">
                        <div class="col-md-12">
                            <!-- Advanced Tables -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                   Tabel Admin
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>USERNAME</th>
                                                    <th>PASSWORD</th>
                                                    <th>Edit</th>
                                                    <th>Hapus</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                include("./content/connect.php");
                                                $sql = mysql_query("select * from user");
                                                $no = 0;
                                                while ($row = mysql_fetch_array($sql)) {
                                                    $no++;
                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td><?php echo $no; ?></td>
                                                        <td class="center"><?php echo $row['username']; ?></td>
                                                        <td class="center"><?php echo $row['password']; ?></td>
                                                        <td class="center"><a href="./?page=update_admin&id=<?php echo $row['id_admin']?>"><button class="btn btn-primary"><i class="fa fa-edit "></i> Edit</button></a></td>
                                                        <td class="center"><a href="./content/delete_admin.php?id=<?php echo $row['id_admin']?>" onclick="return confirm('apakah yakin akan dihapus?')"><button class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button></a></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <!--End Advanced Tables -->
                        </div>
                    </div>
                    <!-- /. ROW  -->
                </div>
                <?php include ("footer.php"); ?>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
        <!-- /. WRAPPER  -->
        <!-- JS Scripts-->
        <!-- jQuery Js -->
        <script src="assets/js/jquery-1.10.2.js"></script>
        <!-- Bootstrap Js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Metis Menu Js -->
        <script src="assets/js/jquery.metisMenu.js"></script>
        <!-- DATA TABLE SCRIPTS -->
        <script src="assets/js/dataTables/jquery.dataTables.js"></script>
        <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
        </script>
        <!-- Custom Js -->
        <script src="assets/js/custom-scripts.js"></script>


    </body>


    <!-- Mirrored from webthemez.com/demo/bluebox-free-bootstrap-admin-template/table.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Apr 2016 07:52:46 GMT -->
</html>
