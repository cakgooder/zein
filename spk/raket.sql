-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 26, 2017 at 04:27 PM
-- Server version: 5.6.30-0ubuntu0.15.10.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `raket`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_test`
--

CREATE TABLE `data_test` (
  `ID_DATA` int(11) NOT NULL,
  `ID_TIPE` varchar(10) DEFAULT NULL,
  `ID_KRITERIA` varchar(30) NOT NULL,
  `ID_SUBKRITERIA` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_test`
--

INSERT INTO `data_test` (`ID_DATA`, `ID_TIPE`, `ID_KRITERIA`, `ID_SUBKRITERIA`) VALUES
(3012, 'YNX1', 'BFM', 'BFM1'),
(3013, 'YNX1', 'BRT', 'BRT1'),
(3014, 'YNX1', 'BTFM', 'BTFM1'),
(3015, 'YNX1', 'FLX', 'FLX1'),
(3016, 'YNX1', 'GRP', 'GRP1'),
(3017, 'YNX1', 'HRG', 'HRG1'),
(3018, 'YNX1', 'JRK', 'JRK1'),
(3019, 'YNX1', 'RGS', 'RGS3'),
(3020, 'YNX10', 'BFM', 'BFM1'),
(3021, 'YNX10', 'BRT', 'BRT2'),
(3022, 'YNX10', 'FLX', 'FLX3'),
(3023, 'YNX10', 'GRP', 'GRP2'),
(3024, 'YNX10', 'HRG', 'HRG1'),
(3025, 'YNX10', 'JRK', 'JRK1'),
(3026, 'YNX10', 'RGS', 'RGS2'),
(3027, 'YNX10', 'BTFM', 'BTFM4'),
(3028, 'YNX11', 'BFM', 'BFM1'),
(3029, 'YNX11', 'BRT', 'BRT1'),
(3030, 'YNX11', 'FLX', 'FLX1'),
(3031, 'YNX11', 'GRP', 'GRP3'),
(3032, 'YNX11', 'HRG', 'HRG1'),
(3033, 'YNX11', 'JRK', 'JRK2'),
(3034, 'YNX11', 'RGS', 'RGS1'),
(3035, 'YNX11', 'BTFM', 'BTFM2'),
(3036, 'YNX13', 'BFM', 'BFM1'),
(3037, 'YNX13', 'BRT', 'BRT1'),
(3038, 'YNX13', 'FLX', 'FLX3'),
(3039, 'YNX13', 'GRP', 'GRP3'),
(3040, 'YNX13', 'HRG', 'HRG1'),
(3041, 'YNX13', 'JRK', 'JRK1'),
(3042, 'YNX13', 'RGS', 'RGS2'),
(3043, 'YNX13', 'BTFM', 'BTFM4'),
(3044, 'YNX14', 'BFM', 'BFM1'),
(3045, 'YNX14', 'BRT', 'BRT1'),
(3046, 'YNX14', 'FLX', 'FLX2'),
(3047, 'YNX14', 'GRP', 'GRP3'),
(3048, 'YNX14', 'HRG', 'HRG1'),
(3049, 'YNX14', 'JRK', 'JRK3'),
(3050, 'YNX14', 'RGS', 'RGS2'),
(3051, 'YNX14', 'BTFM', 'BTFM1'),
(3052, 'YNX15', 'BFM', 'BFM1'),
(3053, 'YNX15', 'BRT', 'BRT2'),
(3054, 'YNX15', 'FLX', 'FLX1'),
(3055, 'YNX15', 'GRP', 'GRP2'),
(3056, 'YNX15', 'HRG', 'HRG2'),
(3057, 'YNX15', 'JRK', 'JRK3'),
(3058, 'YNX15', 'RGS', 'RGS1'),
(3059, 'YNX15', 'BTFM', 'BTFM2'),
(3060, 'YNX16', 'BFM', 'BFM1'),
(3061, 'YNX16', 'BRT', 'BRT1'),
(3062, 'YNX16', 'FLX', 'FLX1'),
(3063, 'YNX16', 'GRP', 'GRP3'),
(3064, 'YNX16', 'HRG', 'HRG2'),
(3065, 'YNX16', 'JRK', 'JRK3'),
(3066, 'YNX16', 'RGS', 'RGS2'),
(3067, 'YNX16', 'BTFM', 'BTFM1'),
(3068, 'YNX17', 'BFM', 'BFM1'),
(3069, 'YNX17', 'BRT', 'BRT1'),
(3070, 'YNX17', 'FLX', 'FLX2'),
(3071, 'YNX17', 'GRP', 'GRP3'),
(3072, 'YNX17', 'HRG', 'HRG1'),
(3073, 'YNX17', 'JRK', 'JRK3'),
(3074, 'YNX17', 'RGS', 'RGS2'),
(3075, 'YNX17', 'BTFM', 'BTFM1'),
(3076, 'YNX12', 'BFM', 'BFM1'),
(3077, 'YNX12', 'BRT', 'BRT2'),
(3078, 'YNX12', 'FLX', 'FLX3'),
(3079, 'YNX12', 'GRP', 'GRP2'),
(3080, 'YNX12', 'HRG', 'HRG4'),
(3081, 'YNX12', 'JRK', 'JRK1'),
(3082, 'YNX12', 'RGS', 'RGS2'),
(3083, 'YNX12', 'BTFM', 'BTFM1'),
(3084, 'YNX3', 'BFM', 'BFM1'),
(3085, 'YNX3', 'BRT', 'BRT1'),
(3086, 'YNX3', 'FLX', 'FLX2'),
(3087, 'YNX3', 'GRP', 'GRP3'),
(3088, 'YNX3', 'HRG', 'HRG1'),
(3089, 'YNX3', 'JRK', 'JRK3'),
(3090, 'YNX3', 'RGS', 'RGS2'),
(3091, 'YNX3', 'BTFM', 'BTFM3'),
(3092, 'YNX4', 'BFM', 'BFM1'),
(3093, 'YNX4', 'BRT', 'BRT1'),
(3094, 'YNX4', 'FLX', 'FLX1'),
(3095, 'YNX4', 'GRP', 'GRP3'),
(3096, 'YNX4', 'HRG', 'HRG1'),
(3097, 'YNX4', 'JRK', 'JRK3'),
(3098, 'YNX4', 'RGS', 'RGS2'),
(3099, 'YNX4', 'BTFM', 'BTFM3'),
(3100, 'YNX5', 'BFM', 'BFM1'),
(3101, 'YNX5', 'BRT', 'BRT1'),
(3102, 'YNX5', 'FLX', 'FLX2'),
(3103, 'YNX5', 'GRP', 'GRP3'),
(3104, 'YNX5', 'HRG', 'HRG1'),
(3105, 'YNX5', 'JRK', 'JRK2'),
(3106, 'YNX5', 'RGS', 'RGS1'),
(3107, 'YNX5', 'BTFM', 'BTFM3'),
(3108, 'YNX6', 'BFM', 'BFM1'),
(3109, 'YNX6', 'BRT', 'BRT1'),
(3110, 'YNX6', 'FLX', 'FLX2'),
(3111, 'YNX6', 'GRP', 'GRP3'),
(3112, 'YNX6', 'HRG', 'HRG1'),
(3113, 'YNX6', 'JRK', 'JRK2'),
(3114, 'YNX6', 'RGS', 'RGS1'),
(3115, 'YNX6', 'BTFM', 'BTFM2'),
(3116, 'YNX7', 'BFM', 'BFM1'),
(3117, 'YNX7', 'BRT', 'BRT1'),
(3118, 'YNX7', 'FLX', 'FLX1'),
(3119, 'YNX7', 'GRP', 'GRP3'),
(3120, 'YNX7', 'HRG', 'HRG1'),
(3121, 'YNX7', 'JRK', 'JRK2'),
(3122, 'YNX7', 'RGS', 'RGS1'),
(3123, 'YNX7', 'BTFM', 'BTFM4'),
(3124, 'YNX8', 'BFM', 'BFM1'),
(3125, 'YNX8', 'BRT', 'BRT1'),
(3126, 'YNX8', 'FLX', 'FLX2'),
(3127, 'YNX8', 'GRP', 'GRP3'),
(3128, 'YNX8', 'HRG', 'HRG2'),
(3129, 'YNX8', 'JRK', 'JRK3'),
(3130, 'YNX8', 'RGS', 'RGS2'),
(3131, 'YNX8', 'BTFM', 'BTFM2'),
(3132, 'YNX9', 'BFM', 'BFM1'),
(3133, 'YNX9', 'BRT', 'BRT1'),
(3134, 'YNX9', 'FLX', 'FLX2'),
(3135, 'YNX9', 'GRP', 'GRP3'),
(3136, 'YNX9', 'HRG', 'HRG4'),
(3137, 'YNX9', 'JRK', 'JRK1'),
(3138, 'YNX9', 'RGS', 'RGS2'),
(3139, 'YNX9', 'BTFM', 'BTFM2'),
(3140, 'YNX2', 'BFM', 'BFM3'),
(3141, 'YNX2', 'BRT', 'BRT3'),
(3142, 'YNX2', 'FLX', 'FLX2'),
(3143, 'YNX2', 'GRP', 'GRP3'),
(3144, 'YNX2', 'HRG', 'HRG3'),
(3145, 'YNX2', 'JRK', 'JRK1'),
(3146, 'YNX2', 'RGS', 'RGS1'),
(3147, 'YNX2', 'BTFM', 'BTFM1');

-- --------------------------------------------------------

--
-- Table structure for table `dethasil`
--

CREATE TABLE `dethasil` (
  `dethasil_id` int(11) NOT NULL,
  `hasil_id` int(11) NOT NULL,
  `tipe_raket` varchar(255) NOT NULL,
  `nilai_ahp` float NOT NULL,
  `rank` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dethasil`
--

INSERT INTO `dethasil` (`dethasil_id`, `hasil_id`, `tipe_raket`, `nilai_ahp`, `rank`) VALUES
(1, 1, 'yonex', 0.54, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hasil`
--

CREATE TABLE `hasil` (
  `hasil_id` int(11) NOT NULL,
  `hasil_nama` varchar(255) NOT NULL,
  `hasil_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `haskrit`
--

CREATE TABLE `haskrit` (
  `haskrit_id` int(11) NOT NULL,
  `hasil_id` int(11) NOT NULL,
  `kriteria_id` varchar(11) NOT NULL,
  `subkriteria_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `haskrit`
--

INSERT INTO `haskrit` (`haskrit_id`, `hasil_id`, `kriteria_id`, `subkriteria_id`) VALUES
(1, 1, 'BRT', 'BRT1'),
(2, 1, 'HRG', 'HRG3'),
(3, 6, 'BFM', 'BFM1'),
(4, 6, 'BRT', 'BRT1'),
(5, 6, 'FLX', 'FLX1'),
(6, 6, 'GRP', 'GRP1'),
(7, 6, 'HRG', 'HRG1'),
(8, 6, 'JRK', 'JRK1'),
(9, 6, 'RGS', 'RGS1'),
(10, 6, 'BTFM', 'BTFM1'),
(11, 7, 'BFM', 'BFM1'),
(12, 7, 'BRT', 'BRT1'),
(13, 7, 'FLX', 'FLX1'),
(14, 7, 'GRP', 'GRP1'),
(15, 7, 'HRG', 'HRG1'),
(16, 7, 'JRK', 'JRK1'),
(17, 7, 'RGS', 'RGS1'),
(18, 7, 'BTFM', 'BTFM1'),
(19, 8, 'BFM', 'BFM1'),
(20, 8, 'BRT', 'BRT1'),
(21, 8, 'FLX', 'FLX1'),
(22, 8, 'GRP', 'GRP1'),
(23, 8, 'HRG', 'HRG1'),
(24, 8, 'JRK', 'JRK1'),
(25, 8, 'RGS', 'RGS1'),
(26, 8, 'WNR', 'WNR1'),
(27, 9, 'BFM', 'BFM1'),
(28, 9, 'BRT', 'BRT1'),
(29, 9, 'FLX', 'FLX1'),
(30, 9, 'GRP', 'GRP1'),
(31, 9, 'HRG', 'HRG1'),
(32, 9, 'JRK', 'JRK1'),
(33, 9, 'RGS', 'RGS1'),
(34, 9, 'WNR', 'WNR1'),
(35, 10, 'BFM', 'BFM2'),
(36, 10, 'BRT', 'BRT1'),
(37, 10, 'FLX', 'FLX3'),
(38, 10, 'GRP', 'GRP2'),
(39, 10, 'HRG', 'HRG2'),
(40, 10, 'JRK', 'JRK2'),
(41, 10, 'RGS', 'RGS2'),
(42, 10, 'WNR', 'WNR5');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `ID_KRITERIA` varchar(30) NOT NULL,
  `NAMA_KRITERIA` varchar(60) DEFAULT NULL,
  `BOBOT` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`ID_KRITERIA`, `NAMA_KRITERIA`, `BOBOT`) VALUES
('BFM', 'Bahan Frame', 10),
('BRT', 'Berat', 10),
('BTFM', 'Bahan Batang Frame', 10),
('FLX', 'Fleksibilitas', 15),
('GRP', 'GRIP', 10),
('HRG', 'Harga', 10),
('JRK', 'Jenis Raket', 20),
('RGS', 'Regangan Senar Raket', 15);

-- --------------------------------------------------------

--
-- Table structure for table `produsen_raket`
--

CREATE TABLE `produsen_raket` (
  `ID_RAKET` varchar(30) NOT NULL,
  `NAMA_PRODUSENRAKET` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produsen_raket`
--

INSERT INTO `produsen_raket` (`ID_RAKET`, `NAMA_PRODUSENRAKET`) VALUES
('YNX', 'Yonex');

-- --------------------------------------------------------

--
-- Table structure for table `subkriteria`
--

CREATE TABLE `subkriteria` (
  `ID_SUBKRITERIA` varchar(30) NOT NULL,
  `ID_KRITERIA` varchar(30) DEFAULT NULL,
  `NAMA_SUBKRITERIA` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subkriteria`
--

INSERT INTO `subkriteria` (`ID_SUBKRITERIA`, `ID_KRITERIA`, `NAMA_SUBKRITERIA`) VALUES
('BFM1', 'BFM', 'Graphite'),
('BFM2', 'BFM', 'Carbon'),
('BFM3', 'BFM', 'Alluminium'),
('BFM4', 'BFM', 'Pilihan Lainnya'),
('BRT1', 'BRT', 'U.   95 - 99 gram'),
('BRT2', 'BRT', '2U. 90 - 95 gram'),
('BRT3', 'BRT', '3U. 85 - 89 gram'),
('BRT4', 'BRT', '4U. 80 - 84 gram'),
('BTFM1', 'BTFM', 'Graphite'),
('BTFM2', 'BTFM', 'Carbon'),
('BTFM3', 'BTFM', 'Alluminium'),
('BTFM4', 'BTFM', 'Pilihan Lainnya'),
('FLX1', 'FLX', 'Flexible'),
('FLX2', 'FLX', 'Medium'),
('FLX3', 'FLX', 'Stiff'),
('GRP1', 'GRP', 'G2 (4")'),
('GRP2', 'GRP', 'G3 (3.75")'),
('GRP3', 'GRP', 'G4 (3.5")'),
('GRP4', 'GRP', 'G5 (3.35")'),
('HRG1', 'HRG', 'Rp.500.000 - Rp.900.000'),
('HRG2', 'HRG', 'Rp.910.000 - Rp.1.300.000 '),
('HRG3', 'HRG', 'Rp.1.310.000 - Rp.1.700.000'),
('HRG4', 'HRG', 'Rp.1.710.000 - Rp.2.000.000'),
('JRK1', 'JRK', 'Attack'),
('JRK2', 'JRK', 'Defensive'),
('JRK3', 'JRK', 'Balance'),
('RGS1', 'RGS', '19 LBS - 24 LBS'),
('RGS2', 'RGS', '20 LBS - 28 LBS'),
('RGS3', 'RGS', '22 LBS - 30 LBS'),
('RGS4', 'RGS', 'Pilihan Lainnya');

-- --------------------------------------------------------

--
-- Table structure for table `tipe_raket`
--

CREATE TABLE `tipe_raket` (
  `ID_TIPE` varchar(10) NOT NULL,
  `ID_RAKET` varchar(30) DEFAULT NULL,
  `NAMA_TIPERAKET` varchar(30) DEFAULT NULL,
  `GAMBAR` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_raket`
--

INSERT INTO `tipe_raket` (`ID_TIPE`, `ID_RAKET`, `NAMA_TIPERAKET`, `GAMBAR`) VALUES
('YNX1', 'YNX', 'Arcsaber 4DX', 'Arcsaber_4DX.jpg'),
('YNX10', 'YNX', 'Voltric 7', 'voltric_7.jpg'),
('YNX11', 'YNX', 'Nanoray 20', 'nanoray_20.jpg'),
('YNX12', 'YNX', 'Nanoray Power 3i', 'nanoray_power_3i_.jpg'),
('YNX13', 'YNX', 'Muscle Power 99', 'muscle_power_99.jpg'),
('YNX14', 'YNX', 'Muscle Power 29 Light', 'mp_29light.jpg'),
('YNX15', 'YNX', 'Arcsaber 6 FL', 'arcsaber_6fl.jpg'),
('YNX16', 'YNX', 'Arcsaber 3 TOUR', 'arcsaber_3_tour.jpg'),
('YNX17', 'YNX', 'MUSCLE POWER 22', 'mp_22.jpg'),
('YNX2', 'YNX', 'Voltric Z-Force II', 'VTZFII_1.jpg'),
('YNX3', 'YNX', 'Arcsaber D-8', 'Arcsaber_d8.jpg'),
('YNX4', 'YNX', 'Arcsaber D-5', 'Arcsaber_d5.jpg'),
('YNX5', 'YNX', 'Nanoray D2', 'nanoray d2.jpg'),
('YNX6', 'YNX', 'Nanoray LPLUS 8', 'Nanoray_LPlus_8.jpg'),
('YNX7', 'YNX', 'Nanoray D22', 'nanoray_D22.jpg'),
('YNX8', 'YNX', 'Arcsaber 8DX', 'arcsaber_8DX.jpg'),
('YNX9', 'YNX', 'Voltric  70 E-tune', 'voltric_70_etune.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_admin`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'Joko', '900150983cd24fb0d6963f7d28e17f72'),
(3, 'jhkb', '5012a1cbc560d21c9ad22c555790c2b0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_test`
--
ALTER TABLE `data_test`
  ADD PRIMARY KEY (`ID_DATA`),
  ADD KEY `FK_RELATIONSHIP_3` (`ID_SUBKRITERIA`),
  ADD KEY `FK_RELATIONSHIP_4` (`ID_TIPE`),
  ADD KEY `FK_RELATIONSHIP_5` (`ID_KRITERIA`);

--
-- Indexes for table `dethasil`
--
ALTER TABLE `dethasil`
  ADD PRIMARY KEY (`dethasil_id`);

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`hasil_id`);

--
-- Indexes for table `haskrit`
--
ALTER TABLE `haskrit`
  ADD PRIMARY KEY (`haskrit_id`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`ID_KRITERIA`);

--
-- Indexes for table `produsen_raket`
--
ALTER TABLE `produsen_raket`
  ADD PRIMARY KEY (`ID_RAKET`);

--
-- Indexes for table `subkriteria`
--
ALTER TABLE `subkriteria`
  ADD PRIMARY KEY (`ID_SUBKRITERIA`),
  ADD KEY `FK_RELATIONSHIP_2` (`ID_KRITERIA`);

--
-- Indexes for table `tipe_raket`
--
ALTER TABLE `tipe_raket`
  ADD PRIMARY KEY (`ID_TIPE`),
  ADD KEY `FK_RELATIONSHIP_1` (`ID_RAKET`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_admin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_test`
--
ALTER TABLE `data_test`
  MODIFY `ID_DATA` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3148;
--
-- AUTO_INCREMENT for table `dethasil`
--
ALTER TABLE `dethasil`
  MODIFY `dethasil_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hasil`
--
ALTER TABLE `hasil`
  MODIFY `hasil_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `haskrit`
--
ALTER TABLE `haskrit`
  MODIFY `haskrit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_test`
--
ALTER TABLE `data_test`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`ID_SUBKRITERIA`) REFERENCES `subkriteria` (`ID_SUBKRITERIA`),
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`ID_TIPE`) REFERENCES `tipe_raket` (`ID_TIPE`),
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`ID_KRITERIA`) REFERENCES `kriteria` (`ID_KRITERIA`);

--
-- Constraints for table `subkriteria`
--
ALTER TABLE `subkriteria`
  ADD CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`ID_KRITERIA`) REFERENCES `kriteria` (`ID_KRITERIA`);

--
-- Constraints for table `tipe_raket`
--
ALTER TABLE `tipe_raket`
  ADD CONSTRAINT `FK_RELATIONSHIP_1` FOREIGN KEY (`ID_RAKET`) REFERENCES `produsen_raket` (`ID_RAKET`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
