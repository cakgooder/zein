<?php
session_start();
?>
<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="ThemeWagon">
        <meta name="Description" content="Rights Non-Profit Template">
        <meta name="keywords" content="non-profit, child">
        <link rel="apple-touch-icon" sizes="57x57" href="./assets/images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="./assets/images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="./assets/images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="./assets/images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="./assets/images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="./assets/images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="./assets/images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="./assets/images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="./assets/images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="./assets/images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="./assets/images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="./assets/images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="./assets/images/favicons/favicon-16x16.png">
        <link rel="shortcut icon" href="./assets/images/favicons/favicon-16x16.png">
        <link rel="manifest" href="./assets/images/favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="./assets/images/favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        
        <title> Pemilihan Racket</title>
        
        <link rel="stylesheet" href="./assets/lib/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/lib/owlcarousel/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="./assets/lib/owlcarousel/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="./assets/lib/ionicons/css/ionicons.css">
        <link rel="stylesheet" href="./assets/lib/fontawesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="./assets/extras/swipebox/css/swipebox.min.css">
        <link rel="stylesheet" href="./assets/extras/rotating-carousel/css/style.css">
        <link rel="stylesheet" href="./assets/extras/slick/slick.css">
        <link rel="stylesheet" href="./assets/extras/magnificpopup/magnific-popup.css">
        <link rel="stylesheet" href="./assets/css/main.css">
        <link rel="stylesheet" href="./assets/css/custom.css">
    </head>
    <body>
        <div id="wrapper">
            <div id="loader">
            </div>
        </div>
        <div class="main_container">
            
            <?php 
            include 'connect.php';
            include 'header.php'; ?>
            
            <?php include 'content.php';?>
            
            <?php include 'footer.php'; ?>
            
        </div>
    </body>
        <script src="./assets/lib/components-modernizr/modernizr.js"></script>
        <script src="./assets/lib/jquery/dist/jquery.js"></script>
        <script src="./assets/lib/bootstrap/dist/js/bootstrap.js"></script>
        <script src="./assets/lib/owlcarousel/owl-carousel/owl.carousel.min.js"></script>
        <script src="./assets/extras/swipebox/js/jquery.swipebox.min.js"></script>
        <script src="./assets/extras/rotating-carousel/js/jquery.gallery.js"></script>
        <script src="./assets/extras/slick/slick.js"></script>
        <script src="./assets/extras/magnificpopup/jquery.magnific-popup.min.js"></script>
        <script src="./assets/extras/scrollspeed/jQuery.scrollSpeed.js"></script>
        <script src="./assets/lib/waypoints/lib/jquery.waypoints.min.js"></script>
        <script src="./assets/lib/waypoints/lib/shortcuts/inview.min.js"></script>
        <script src="./assets/lib/countdown/dest/jquery.countdown.min.js"></script>
        <script src="./assets/lib/isotope/dist/isotope.pkgd.min.js"></script>
        <script src="./assets/js/main.js"></script>
</html>