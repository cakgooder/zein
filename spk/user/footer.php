
<footer id=footer class="section footer">
    <div class=container>
        <div class=row>
            <div class="col-md-6 col-sm-6">
                <p class=copyright>&copy; 2016 Rexananda's.Co</p>
            </div>
            <div class="col-md-6 col-sm-6">
                <ul class=footer-menu>
                    <li><a>Terms &amp; Conditions</a></li>
                    <li><a href="#">Privacy Policy</a></li><!-- 
                    <li><a href=https://themewagon.com/>Theme by ThemeWagon</a></li> -->
                </ul>
            </div>
        </div>
    </div>
</footer>