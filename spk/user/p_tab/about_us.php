<!DOCTYPE html>


            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/about.png); background-position-y: center;">
                <div class=overlay-01>

                </div>
                <div class=container>
                    <h2 class="text-center text-uppercase">About Us</h2>
                    <div class=breadcrumb>
                        <a href="#">Home</a> 
                        <span>/</span> 
                        <a href="#" class=page-active>About Us</a>
                    </div>
                </div>
            </header>
            <section id=about class="small-top-pad section bg-default">
                <div class=container>
                    <div class=row>
                        <div class=col-md-12>
                            <h3 class=story-title>Our Success story</h3>
                        </div>
                        <div class=col-md-5>
                            <div class=success-story-video style="background-image: url(assets/images/about-us/video-player.png)">
                                <a class=popup-vimeo href=http://vimeo.com/45830194>
                                    <i class="fa fa-play-circle-o"></i>
                                </a>
                            </div>
                        </div>
                        <div class=col-md-7>
                            <div class=success-story>
                                <p class=story>Duis sit amet sapien consequat, commodo lacus nec, porta o. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac tegestas ase Quisque elit purus, iaculis sed nunc eu, imperdiet facilisis arcu. Donec eget rutrum lacus, in fermentum augue. Aliquam tristique rutrum felis eleifende is a lacus tempor sed Duis sit amet sapien consequat, commodo lacus nec, porta o. Pellentesque habitant morbi.</p>
                                <p class=story-last>Nam eleifend mollis leo, eu tristique nibh scelerisque at. Cras bibendum enim nec diam consequat. ipsum rhoncus, vitae eleifend quam congue. Fusce at sapien quis purus gravida varius ut sed turpis. Ut elit elit, malesuada id purus diam consequateg.</p>
                            </div>
                        </div>
                        <div class=col-md-12>
                            <div class=success-story-img>
                                <img class=img-responsive src=assets/images/about_us_story.png alt="Success Story">
                            </div>
                        </div>
                        <div class=col-md-6>
                            <div class=success-story-img-content>
                                <p>Duis sit amet sapien consequat, commodo lacus nec, porta o. Pellentesquei habitant morbi tristique senectus et netus et malesuada fames ac tegestasi ase Quisque elit purus, iaculis sed nunc eu, imperdiet facilisis arcu. Donece eget rutrum lacus, in fermentum augue. Aliquam tristique rutrum felis erse eleifende is a lacus tempor sed Duis sit amet sapien consequat.</p>
                            </div>
                        </div>
                        <div class=col-md-6>
                            <div class=success-story-img-content>
                                <p>Duis sit amet sapien consequat, commodo lacus nec, porta o. Pellentesquei habitant morbi tristique senectus et netus et malesuada fames ac tegestasi ase Quisque elit purus, iaculis sed nunc eu, imperdiet facilisis arcu. Donece eget rutrum lacus, in fermentum augue. Aliquam tristique rutrum felis erse eleifende is a lacus tempor sed Duis sit amet sapien consequat.</p>
                                <div>
                                    
                                </div>
                                    
                            </div>
                                
                        </div>
                            
                    </div>
                        
                </div>
            </section>
            <section id=team class="section bg-default">
                <div class=container>
                    <div class=row>
                        <div class=col-md-12>
                            <h3 class=meet-us-heading>MEET US</h3>
                            <div class=row>
                                <div class="col-md-4 col-sm-6">
                                    <div class="team-wiget clearfix">
                                        <img class=img-responsive src=assets/images/team/meet_us_1.png alt=Image>
                                        <div class=meet-social>
                                            <a href=#><i class="fa fa-facebook-f"></i></a> 
                                            <a href=#><i class="fa fa-twitter"></i></a> 
                                            <a href=#><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class=personal-info>
                                            <p class=title>Jenne Enna</p>
                                            <p class=designation>Chairperson of Charty org</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="team-wiget clearfix">
                                        <img class=img-responsive src=assets/images/team/meet_us_2.png alt=Image>
                                        <div class=meet-social>
                                            <a href=#><i class="fa fa-facebook-f"></i></a> 
                                            <a href=#><i class="fa fa-twitter"></i></a> 
                                            <a href=#><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class=personal-info>
                                            <p class=title>Jenne Enna</p>
                                            <p class=designation>Chairperson of Charty org</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6">
                                    <div class="team-wiget clearfix">
                                        <img class=img-responsive src=assets/images/team/meet_us_3.png alt=Image>
                                        <div class=meet-social>
                                            <a href=#><i class="fa fa-facebook-f"></i></a> 
                                            <a href=#><i class="fa fa-twitter"></i></a> 
                                            <a href=#><i class="fa fa-linkedin"></i></a>
                                        </div>
                                        <div class=personal-info>
                                            <p class=title>Jenne Enna</p>
                                            <p class=designation>Chairperson of Charty org</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6"><div class="team-wiget clearfix"><img class=img-responsive src=assets/images/team/meet_us_4.png alt=Image><div class=meet-social><a href=#><i class="fa fa-facebook-f"></i></a> <a href=#><i class="fa fa-twitter"></i></a> <a href=#><i class="fa fa-linkedin"></i></a></div><div class=personal-info><p class=title>Jenne Enna</p><p class=designation>Chairperson of Charty org</p></div></div></div>
                                <div class="col-md-4 col-sm-6"><div class="team-wiget clearfix"><img class=img-responsive src=assets/images/team/meet_us_5.png alt=Image><div class=meet-social><a href=#><i class="fa fa-facebook-f"></i></a> <a href=#><i class="fa fa-twitter"></i></a> <a href=#><i class="fa fa-linkedin"></i></a></div><div class=personal-info><p class=title>Jenne Enna</p><p class=designation>Chairperson of Charty org</p></div></div></div>
                                <div class="col-md-4 col-sm-6"><div class="team-wiget clearfix"><img class=img-responsive src=assets/images/team/meet_us_6.png alt=Image><div class=meet-social><a href=#><i class="fa fa-facebook-f"></i></a> <a href=#><i class="fa fa-twitter"></i></a> <a href=#><i class="fa fa-linkedin"></i></a></div><div class=personal-info><p class=title>Jenne Enna</p><p class=designation>Chairperson of Charty org</p></div></div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>