<!DOCTYPE html>
            
            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/login.png); background-position-y: justify;">
                <div class="overlay-01">
                </div>
                <div class="container">
                    <h2 class="text-center text-uppercase">Login</h2>
                    <div class=breadcrumb>
                        <a href=#>Home</a> 
                        <span>/</span> 
                        <a href=# class=page-active>Login</a>
                    </div>
                </div>
            </header>  
           	<section id=component_forms class="section bg-default pad-regular">
                <div class="container form-preview">
                    <div class=row>
                        <div class="col-sm-8 col-sm-offset-2">
                         
                        <form class="form-login" method="post" action="p_login.php">

                                <h2 class="form-login-heading">SIGN IN</h2>
                                <div class="login-wrap">
                                    <input type="text" class="form-control" placeholder="User ID" name="username" autofocus>
                                    <br>
                                    <input type="password" class="form-control" placeholder="Password" name="password">
                                    <label class="checkbox">
                                        <span class="pull-right">
                                            <a class="btn btn-theme btn-charity-default " href=""> Forgot Password?</a>

                                        </span>
                                    </label>
                                    <button class="btn btn-theme btn-success" href="#" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
                                    <hr>

                                    <div class="registration">
                                        Don't have an account yet?<br/>
                                        <a class="btn btn-theme btn-subscribe" href="#">
                                            Create an account
                                        </a>
                                    </div>

                                </div>

                            </form>	  	
                        </div>
                    </div>
                </div>
            </section>
            