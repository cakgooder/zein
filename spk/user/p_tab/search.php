
            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/gallery.png); background-position-y: top;">
                    <div class="overlay-01">
                </div>
                <div class="container">
                     <h2 class="text-center text-uppercase">Gallery</h2>
                    <div class="breadcrumb">
                        <a href=#>Home</a> 
                        <span>/</span> <a href=# class="page-active">Gallery</a>
                    </div>
                </div>
            </header>
            <section id="gallery" class="section gallery pad-regular bg-default">
                <div class=container>
                    <div class="normal_heading">
                        <h2>Raket Gallery</h2>
                    </div>
                    <div id="sortable_gallery">
                        <ul class="gallery-filter">
                            <li class="current">
                                <a class="btn btn-sm" href="#" data-filter=*>All</a>
                            </li>
                            <?php
                            $prod_raket=mysqli_query($koneksidb, "SELECT * FROM produsen_raket");
                            while ($get_prod=mysqli_fetch_array($prod_raket)) {
                            ?>
                            <li>
                                <a class="btn btn-sm current" href="#" data-filter=.<?php echo $get_prod['NAMA_PRODUSENRAKET'];?>><?php echo ucfirst(strtolower($get_prod['NAMA_PRODUSENRAKET']));?></a>
                            </li>
                            <?php
                            }
                            ?>
                        </ul>

                        <form class="form-horizontal" action="index.php?page=search" method="post">
                                <div class="form-group">
                                    <label class="col-md-3" align="right">Cari</label>
                                    <div class="col-md-6">
                                        <input type="text" name="cari" class="col-md-6">
                                        <button type="submit" class="button green">cari</button>
                                    </div>
                                </div>
                            </form>
                        <div class="row">
                            <div class="col-md-12 gallery-grid gallery-3-col">
                                <?php
                                $cari="%".$_POST['cari']."%";
                                $raket=mysqli_query($koneksidb, "SELECT * FROM tipe_raket WHERE nama_tiperaket LIKE '$cari' ");
                              
                                while($getraket=mysqli_fetch_array($raket)){
                                    $prod_raket_id=mysqli_query($koneksidb, "SELECT * FROM produsen_raket WHERE ID_RAKET='".$getraket['ID_RAKET']."'");
                                    while ($get_prod_id=mysqli_fetch_array($prod_raket_id)) {
                                        $kategor=$get_prod_id['NAMA_PRODUSENRAKET'];
                                    }
                                ?>
                                <style>
                                    .img-responsive{
                                        width:370px;
                                        height:260px;
                                    }
                                </style>
                                <div class="item <?php echo $kategor?>">
                                    <a href="index.php?page=detail_galeri&gambar=<?php echo $getraket['ID_TIPE']?>">
                                        <img class="img-responsive" src="assets/images/gallery/<?php echo $getraket['GAMBAR'];?>"> 
                                        <span class="img-hover"></span>
                                        <h5><?php echo $getraket['NAMA_TIPERAKET'];?></h5>
                                    </a>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>