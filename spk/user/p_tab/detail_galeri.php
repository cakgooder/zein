
            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/gallery.png); background-position-y: top;">
                    <div class="overlay-01">
                </div>
                <div class="container">
                     <h2 class="text-center text-uppercase">Gallery</h2>
                    <div class=breadcrumb>
                        <a href=#>Home</a> 
                        <span>/</span> <a href=# class=page-active>Gallery</a>
                    </div>
                </div>
            </header>
            <?php
            $id=$_GET['gambar'];
            ?>
            <section id=gallery class="section gallery pad-regular bg-default">
                <div class=container>
                    <div class=normal_heading>
                        <h2>Detail Data Gambar</h2>
                    </div>
                    <a href="index.php?page=galeri">
                        <button class="btn btn-warning" style="font-size: 16px; font-weight: bold; margin: 25px">Kembali</button>
                    </a>
                    <?php
                    $raket=mysqli_query($koneksidb, "SELECT * FROM tipe_raket WHERE ID_TIPE='".$id."'");
                    while($getraket=mysqli_fetch_array($raket)){
                        $prod_raket_id=mysqli_query($koneksidb, "SELECT * FROM produsen_raket WHERE ID_RAKET='".$getraket['ID_RAKET']."'");
                        while ($get_prod_id=mysqli_fetch_array($prod_raket_id)) {
                            $kategor=$get_prod_id['NAMA_PRODUSENRAKET'];
                        }
                    ?>
                    <div class="rows">
                        <div class="col-md-6">
                            <a class=lightbox href="assets/images/gallery/<?php echo $getraket['GAMBAR'];?>">
                                <img class=img-responsive src="assets/images/gallery/<?php echo $getraket['GAMBAR'];?>"> 
                                <span class=img-hover></span>
                            </a>
                        </div>
                        <div class="col-md-6">
                            <table>
                            <tr>
                                <td class="col-md-4">Nama</td>
                                <td class="col-md-8">: <?php echo $getraket['NAMA_TIPERAKET']?></td>
                            </tr>
                            <?php
                            $kriteria=mysqli_query($koneksidb, "SELECT * FROM kriteria");
                             $hrg =mysqli_query($koneksidb, "SELECT * FROM data_test a, tipe_raket b WHERE b.ID_TIPE='".$id."' AND a.ID_TIPE=b.ID_TIPE");
                             $gethrg = mysqli_fetch_array($hrg);
                            // $harga = $gethrg['HARGA'];
                            while($getkriteria=mysqli_fetch_array($kriteria)){
                            ?>
                            <tr>
                                <td class="col-md-4">
                                    <?php echo $getkriteria['NAMA_KRITERIA'];?>
                                   <!--  <?php echo $getkriteria['ID_KRITERIA'];?> -->
                                </td>
                                <?php
                                $test=mysqli_query($koneksidb, "SELECT * FROM data_test a, subkriteria b WHERE a.ID_TIPE='".$id."' and a.ID_SUBKRITERIA=b.ID_SUBKRITERIA");
                                while ($gettest=mysqli_fetch_array($test)) {
                                    $sub=$gettest['ID_SUBKRITERIA'];
                                    $subkrit=mysqli_query($koneksidb, "SELECT * FROM subkriteria WHERE ID_KRITERIA='".$getkriteria['ID_KRITERIA']."' AND ID_SUBKRITERIA='".$sub."'");
                                    
                                    while ($getsubkrit=mysqli_fetch_array($subkrit)) {
                                        if ($getsubkrit['ID_KRITERIA'] = "HRG") {
                                            $subkriteria=$getsubkrit['NAMA_SUBKRITERIA'];
                                        }else{
                                           $subkriteria=$harga;
                                        }
                                        
                                    }
                                }
                                ?>
                                <td class="col-md-8">
                                    : <?php echo $subkriteria;?>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                            </table>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </section>