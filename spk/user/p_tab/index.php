<!DOCTYPE html>
<!--<html dir=ltr lang=en-US>-->
    <!-- Mirrored from d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/index_static_classic.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 06:48:29 GMT -->

            <header id=site-header class="site-header classic header-classic wrapper-table">
                <div style="background-image: url(assets/images/headers/Home.jpg);" class="color-overlay parallax-hero bg" data-top="transform: translate3d(0px, 0px, 0px)" data-top-bottom="transform: translate3d(0px, -200px, 0px)" data-anchor-target=#site-header>

                </div>
                <div class=valign-center>
                    <div class=container>
                        <div class="intro text-center">
                            <div class="intro-text-wrap text-left">
                                <h1>PEMILIHAN RAKET BADMINTON</h1>
                                <p class=subtitle>Untuk Semua Kalangan</p>
                            </div>
                            <div class=arrow-down>
                                <a href=#our_stories class=onPageNav>
                                    <i class=ion-ios-arrow-down></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section id=our_stories class="section pad-regular bg-default our_stories small-top-pad">
                <div class=container>
                    <div class="row bg-dark">
                        <div class="col-md-5 col-sm-12 no_left_pad no_right_pad">
                            <img class="img-responsive" style=" width:871px;height:521px;background-color:dark grey;" src=assets/images/rkt.jpg>
                        </div>
                        <div class="col-md-7 col-sm-12 our_story_content text-center">
                            <div class=featured-heading>
                                <h2>Penjelasan Sistem</h2>
                            </div>
                            <table>
                                <tr>
                                    <td>
                                        <p>
                                        Dalam menerapkan pemilihan raket badminton pada setiap orang itu berbeda-beda dengan banyak faktor yang menjadi pembeda. 
                                    Oleh karena itu, orang akan merasakan kesulitan terutama bagi orang awam dalam memilih dan menggunakan raket badminton yang sesuai dengan mereka   . 
                                    Perancangan sistem pendukung keputusan untuk pemilihan raket badminton ini dilakukan untuk membantu semua orang yang mengalami kebingungan dalam menentukan raket badminton yang akan digunakan. 
                                    Oleh karena itu, dibutuhkan suatu sistem yang dapat membantu pemilihan raket badminton untuk memberikan rekomendasi pemilihan raket badminton dimana unsur tersebut memiliki nilai kualitatif.
                                        </p>
                                    </td>
                                </tr>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </section>
            <section id=our_missions class="section pad-regular bg-light our_missions">
                <div class="container">
                    <div class="row">
                        <div class="featured-heading text-center">
                            <h2 class="dark_color">Misi Dan Visi</h2>
                        </div>
                        <div class="col-sm-12 our_mission_content text-center">
                            <blockquote>
                                <p>Membantu dalam memilih raket badminton dengan sistem rekomendasi  pemilihan raket badminton.</p>
                                </br></br>
                                <p>Mewujudkan suatu sistem rekomendasi pemilihan raket badminton yang dapat mempermudah dalam proses memilih raket badminton</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </section>
<!--            <section id=newsletter_card class="section bg-light pad-regular newsletter_card">
                <div class=container>
                </div>
            </section>-->
<!--    </body>-->
    <!-- Mirrored from d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/index_static_classic.php by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 06:48:29 GMT -->
<!--</html>-->