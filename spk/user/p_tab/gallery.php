<!DOCTYPE html><html dir=ltr lang=en-US>
    <!-- Mirrored from d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/gallery.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 06:48:59 GMT -->
    <head>
        <meta charset=UTF-8>
        <meta http-equiv=X-UA-Compatible content="IE=edge">
        <meta name=viewport content="width=device-width, initial-scale=1">
        <meta name=author content=ThemeWagon>
        <meta name=Description content="Rights Non-Profit Template">
        <meta name=keywords content="non-profit, child">
        <title>Gallery | Racket</title>    
        </head>
    <body>
        <div id="wrapper">
            <div id="loader">
            </div>
        </div>
        <div class="main_container">
            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/gallery.png); background-position-y: top;">
                    <div class="overlay-01">
                </div>
                <div class="container">
                     <h2 class="text-center text-uppercase">Gallery</h2>
                    <div class="breadcrumb">
                        <a href="#">Home</a> 
                        <span>/</span> <a href="#" class="page-active">Gallery</a>
                    </div>
                </div>
            </header>
            <section id="gallery" class="section gallery pad-regular bg-default">
                <div class="container">
                    <div class="normal_heading">
                        <h2>Gallery</h2>
                    </div>
                    <div id="sortable_gallery">
                        <ul class="gallery-filter">
                            <li class="current">
                                <a class="btn btn-sm" href="#" data-filter="*">All</a>
                            </li>
                            <li>
                                <a class="btn btn-sm current" href="#" data-filter=".children">Children</a>
                            </li>
                            <li>
                                <a class="btn btn-sm" href="#" data-filter=".education">Education</a>
                            </li>
                            <li>
                                <a class="btn btn-sm" href="#" data-filter=".nature">Nature</a>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="col-md-12 gallery-grid gallery-3-col">
                                <div class="item children">
                                    <a class="lightbox" href="assets/images/gallery/gallery-1.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-1.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item children">
                                    <a class="lightbox" href="assets/images/gallery/gallery-2.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-2.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item education">
                                    <a class="lightbox" href="assets/images/gallery/gallery-3.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-3.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item children">
                                    <a class="lightbox" href="assets/images/gallery/gallery-4.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-4.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item education">
                                    <a class="lightbox" href="assets/images/gallery/gallery-5.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-5.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item education">
                                    <a class="lightbox" href="assets/images/gallery/gallery-6.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-6.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item education">
                                    <a class="lightbox" href="assets/images/gallery/gallery-7.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-7.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item nature">
                                    <a class="lightbox" href="assets/images/gallery/gallery-8.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-8.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item nature">
                                    <a class="lightbox" href="assets/images/gallery/gallery-9.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-9.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item nature">
                                    <a class="lightbox nature" href="assets/images/gallery/gallery-4.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-4.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                                <div class="item nature">
                                    <a class="lightbox" href="assets/images/gallery/gallery-1.png">
                                        <img class="img-responsive" src="assets/images/gallery/gallery-1.png">
                                        <span class="img-hover"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<!--            <section id="call-to-action-small" class="call-to-action-small">
                <div class="container>
                    <div class="row">
                        <div class="col-md-8 text-right">
                            <h3>Let's start doing your bit for the world. Join us as a Volunteer</h3>
                        </div>
                        <div class="col-md-4>
                            <div class="button-align text-left">
                                <a href="#"services class="btn btn-charity-default">Become a volunteer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>-->
<!--            <section class="footer-widgets pad-extra">
                <div class="container>
                    <div class="row">
                        <div class="col-md-4>
                            <div class="widgets-content footer-widget-wrap">
                                <h3 class="widgets-title>About Rights</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur is a adipiscing elit. Nulla convallis egestas oncus. Donec facilisis fermentum sem, ac viverra ante luctus vel.</p>
                                <div class="footer-social>
                                    <a href="#"><i class="fa fa-facebook-f"></i></a> 
                                    <a href="#"><i class="fa fa-twitter"></i></a> 
                                    <a href="#"><i class="fa fa-linkedin"></i></a> 
                                    <a href="#"><i class="fa fa-google-plus"></i></a> 
                                    <a href="#"><i class="fa fa-instagram"></i></a> 
                                    <a href="#"><i class="fa fa-youtube-play"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4>
                            <div class="footer-widget-wrap>
                                <div class="row">
                                    <div class="col-md-11 col-md-offset-1">
                                        <h3 class="widgets-title>quick links</h3>
                                    </div>
                                    <div class="col-md-5 col-md-offset-1">
                                        <div class="widgets-content>
                                            <ul class="widgets-list>
                                                <li><a href="#">Causes</a></li>
                                                <li><a href="#">Projected</a></li>
                                                <li><a href="#">Gallery</a></li>
                                                <li><a href="#">Solution</a></li>
                                                <li><a href="#">Sports</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6>
                                        <div class="widgets-content>
                                            <ul class="widgets-list>
                                                <li><a href="#">Terms of use</a></li>
                                                <li><a href="#">Legal</a></li>
                                                <li><a href="#">Privacy Policy</a></li>
                                                <li><a href="#">Latest News</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4>
                            <div class="widgets-content>
                                <h3 class="widgets-title>Contact Info</h3>
                                <div class="office map">
                                    <p><span>Head office</span><br>Address: 8 king street, Melbourne 89 AUS<br>Phone: +22 6 4567 8900</p>
                                    <p><span>Sub office</span><br>Address: 8 queen street, California 89 USA<br>Phone: +22 6 4567 8900</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <footer id=footer class="section footer">
                <div class="container>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <p class="copyright>&copy; 2015 Your company name</p>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <ul class="footer-menu>
                                <li><a>Terms &amp; Conditions</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="https://themewagon.com/>Theme by ThemeWagon</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>-->
    </body>
    <!-- Mirrored from d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/gallery.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 06:49:12 GMT -->
</html>