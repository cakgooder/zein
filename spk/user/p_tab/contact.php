
<!DOCTYPE html>

            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/Contact_Us.png); background-position-y: center;">
                <div class=overlay-01>
                </div>
                <div class=container>
                    <h2 class="text-center text-uppercase">CONTACT</h2>
                    <div class=breadcrumb>
                        <a href="#">Home</a> 
                        <span>/</span> 
                        <a href="#" class=page-active>CONTACT</a>
                    </div>
                </div>
            </header>
            <section id=contact class="pad-regular section bg-default">
                <div class=container>
                    <div class=row>
                        <div class=col-xs-12>
                            <div class=contactus-brief>
                                <h3>Contact us</h3>
                                <p class=section-description>Duis sit amet sapien consequat, commodo lacus nec, porta odio. Pellentesque habitant morbi.</p>
                            </div>
                        </div>
                    </div>
                    <div class=row>
                        <div class="col-md-12 clearfix">
                            <div id=map-canvas class=map-default-height></div></div></div></div></section>
            <section id=contact-wiget class="section contact-us-wiget-section bg-default">
                <div class=container>
                    <div class=row>
                        <div class="col-md-4 col-sm-6">
                            <div>
                                <div class=contact-wigets-area>
                                    <h4 class=contact-wigets-heading>Texas</h4>
                                    <div class=contact-wigets-description>
                                        <p>Care Boulevard 325, 10000 New York, US</p>
                                        <p>Tel. 01 22 345 67 89 - Fax. 01 23 456 78 90</p>
                                        <p>new.york@carecenter.com</p>
                                        <p></p>

                                    </div><a href="#" class="btn btn-charity-dark-o">
                                        <i class="fa fa-search-plus icon-on-btn"></i>VIEW ON MAP</a>

                                </div>

                            </div>

                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div>
                                <div class=contact-wigets-area>
                                    <h4 class=contact-wigets-heading>Child Law</h4>
                                    <div class=contact-wigets-description>
                                        <p>300 W. 15th Street Austin, TX 78701</p>
                                        <p>Tel. (512) 463-1250 - Fax. 01 23 456 78 90</p>
                                        <p>new.york@carecenter.com</p>

                                    </div><a href="#" class="btn btn-charity-dark-o"><i class="fa fa-search-plus icon-on-btn"></i>VIEW ON MAP</a>

                                </div>

                            </div>

                        </div>
                        <div class="col-md-4 col-sm-6">
                            <div>
                                <div class=contact-wigets-area>
                                    <h4 class=contact-wigets-heading>California</h4>
                                    <div class=contact-wigets-description>
                                        <p>300 W. 15th Street Austin, TX 78701</p>
                                        <p>Tel. (512) 463-1250 - Fax. 01 23 456 78 90</p>
                                        <p>new.york@carecenter.com</p>

                                    </div><a href="#" class="btn btn-charity-dark-o"><i class="fa fa-search-plus icon-on-btn"></i>VIEW ON MAP</a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </section>
            <section id=contact-form class="section bg-default default-section-padding">
                <div class=container>
                    <div class=row>
                        <div class=contact-form>
                            <form action=https://d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/php-files/contact-form.php id=contactForm method=post accept-charset=utf-8><div class=col-md-4>
                                    <div class=form-group><label for=InputName class=dark-text>Your Display Name<super>*<super></super></super></label> 
                                        <input type=text class="form-control form-control-dark" name=name id=InputName placeholder="Jane Doe" required>

                                    </div>

                                </div>
                                <div class=col-md-4>
                                    <div class=form-group>
                                        <label for=InputEmail class=dark-text>Your Email Address<super>*<super></super></super></label>
                                        <input type=email class="form-control form-control-dark" name=email id=InputEmail placeholder=john@example.com required>
                                    </div>
                                </div>
                                <div class=col-md-4>
                                    <div class=form-group>
                                        <label for=InputPhoneNumber class=dark-text>Your Phone Number</label>
                                        <input type=number class="form-control form-control-dark" name=contact_number id=InputPhoneNumber placeholder=123-456-789>
                                    </div>
                                </div>
                                <div class=col-sm-12>
                                    <div class=form-group>
                                        <label for=message class=dark-text>Your Message<super>*<super></super></super></label>
                                        <textarea id=message class="form-control form-control-dark" rows=3 name=message placeholder="Your Message Here.." required></textarea>

                                    </div>
                                    <input id=cfsubmit type=submit name=submit value="SEND MESSAGE" href="#" class="btn btn-charity-default">

                                </div>
                                <div id=contactFormResponse>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        