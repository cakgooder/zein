<?php

$pages_dir = 'p_tab';
if (!empty($_GET['page'])) {
    $pages = scandir($pages_dir);
    $p = $_GET['page'];
    if (in_array($p . '.php', $pages)) {
        include($pages_dir . '/' . $p . '.php');
    } else {
        $pages_dir = 'p_hitung';
        if (!empty($_GET['page'])) {
            $pages = scandir($pages_dir);
            $p = $_GET['page'];
            if (in_array($p . '.php', $pages)) {
                include($pages_dir . '/' . $p . '.php');
            }
        } else {
            echo "404 Halaman tidak ditemukan";
        }
    }
} else {
    include "./$pages_dir/index.php";
}
?>