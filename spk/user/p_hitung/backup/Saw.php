
<?php 

/**
* aIProject
*/
class Saw
{
  
  private $ukuran=0;
  private $skalaTFN=[];
  private $kolom = [];
  private $jumlahTFN=[];
  private $inversTFN=[];
  private $sea=[];
  private $fuzzySynteticExtend=[];
  private $minimum=[];
  private $bobotVektor=[];
  function __construct()
  {
    
  }

  public function setMatrix($matrix=[],$kolom=[])
  {
    $this->setUkuran($matrix);
    $this->setKolom($kolom);
    $this->generateSkalaTFN($matrix);
    $this->setJumlahTFN();
    $this->setInversTFN();
    $this->setSea();
    $this->setFuzzySynteticExtend();
    $this->minimum();
    $this->setBobotVektor();
  }

  public function setUkuran($matrix) {
        $this->ukuran = count($matrix);
    }

    public function getUkuran() {
        return $this->ukuran;
    }

     public function setKolom($kolom)
    {
        $this->kolom=$kolom;
    }

    public function getKolom()
    {
        return $this->kolom;
    }

  public function generateSkalaTFN($matrix)
  {
    $temp=[];
    $i=0;
    foreach ($this->getKolom() as $key) {
            $temp[]=$key;
            $j=0;
            foreach ($this->getKolom() as $key2) {
                if($key==$key2){
                    $skalaTFN[$key."low"][$key2]=1;
                   $skalaTFN[$key."middle"][$key2]=1;
                   $skalaTFN[$key."upper"][$key2]=1;

                }else{

                    
                    if($j>$i){
                     
                      $skala=$this->skalaFuzzy($matrix[$key][$key2]);
                      
                     $skalaTFN[$key."low"][$key2]=$skala[0];
                     $skalaTFN[$key."middle"][$key2]=$skala[1];
                     $skalaTFN[$key."upper"][$key2]=$skala[2];
                     $skalaTFN[$key2."low"][$key]=1/$skala[2];
                     $skalaTFN[$key2."middle"][$key]=1/$skala[1];
                     $skalaTFN[$key2."upper"][$key]=1/$skala[0];
                  }
                 
              }
              $j++;
             
            }
            $i++;
             
        }
        $this->skalaTFN=$skalaTFN;
  }

  public function getSkalaTFN()
  {
    return $this->skalaTFN;
  }

 public function setJumlahTFN()
 {  
    foreach ($this->getKolom() as $key ) {
      $jumlahTFN[$key]["low"]=array_sum($this->getSkalaTFN()[$key."low"]);
      $jumlahTFN[$key]["middle"]=array_sum($this->getSkalaTFN()[$key."middle"]);
      $jumlahTFN[$key]["upper"]=array_sum($this->getSkalaTFN()[$key."upper"]);
    }
    $this->jumlahTFN=$jumlahTFN;
 }
 public function getJumlahTFN()
 {
   return $this->jumlahTFN;
 }

 public function setInversTFN()
 {
  $low=0;$middle=0;$upper=0;
   foreach ($this->getKolom() as $key ) {
     $low+=$this->getJumlahTFN()[$key]['low'];
     $middle+=$this->getJumlahTFN()[$key]['middle'];
     $upper+=$this->getJumlahTFN()[$key]['upper'];
   }
   $this->inversTFN["low"]=1/$low;
   $this->inversTFN["middle"]=1/$middle;
   $this->inversTFN["upper"]=1/$upper;
 }
 public function getInversTFN()
 {
   return $this->inversTFN;
 }

 public function setSea()
 {
    $sea=[];
   foreach ($this->getKolom() as $key) {
     $sea[$key]['low']=$this->getJumlahTFN()[$key]["low"]*$this->getInversTFN()['upper'];
     $sea[$key]['middle']=$this->getJumlahTFN()[$key]["middle"]*$this->getInversTFN()['middle'];
     $sea[$key]['upper']=$this->getJumlahTFN()[$key]["upper"]*$this->getInversTFN()['low'];
   }
   $this->sea=$sea;
 }
 public function getSea()
 {
   return $this->sea;;
 }

 public function setFuzzySynteticExtend()
 {
  $FZE=[];
   foreach ($this->getKolom() as $key ) {
     foreach ($this->getKolom() as $key2 ) {
       if($key==$key2){
        $FZE[$key][$key2]=0;
       }else{
          if($this->getSea()[$key2]["middle"]>=$this->getSea()[$key]["middle"]){
            $FZE[$key][$key2]=1;
          }elseif ($this->getSea()[$key]["low"]>=$this->getSea()[$key2]["upper"]) {
            $FZE[$key][$key2]=0;
          }else{
            $FZE[$key][$key2]=
            ($this->getSea()[$key]["low"]-$this->getSea()[$key2]["upper"])/
            (($this->getSea()[$key2]["middle"]-$this->getSea()[$key2]["upper"])-
            ($this->getSea()[$key]["middle"]-$this->getSea()[$key]["low"]));
          }
       }
     }
   }
   $this->fuzzySynteticExtend=$FZE;
 }

 public function getFuzzySynteticExtend()
 {
   return $this->fuzzySynteticExtend;
 }

 public function minimum()
 {
    $minimum=[];

   foreach ($this->getKolom() as $key) {
     $nilai=10000;
     foreach ($this->getKolom() as $key2) {
       if($nilai>$this->getFuzzySynteticExtend()[$key2][$key] and $this->getFuzzySynteticExtend()[$key2][$key]!= 0){
        $nilai=$this->getFuzzySynteticExtend()[$key2][$key];
       }
     }
     $minimum[$key]=$nilai;
   }
   $this->minimum=$minimum;
 }

 public function getMinimum()
 {
   return $this->minimum;
 }
 public function setBobotVektor()
 {
    $bv=[];
    $jumlah=array_sum($this->getMinimum());
    foreach ($this->getKolom() as $key) {
      echo $key;
       $bv[$key]=$this->getMinimum()[$key]/$jumlah;
     } 
     $this->bobotVektor=$bv;
 }
 public function getBobotVektor()
 {
   return $this->bobotVektor;
 }
  public function skalaFuzzy($value)
  {
    $value=(double) $value;
    $value=round($value,5);
    $skala=[];
    if($value==1){
      $skala=[1,1,3];
    }elseif ($value==3) {
     $skala=[1,3,5];
    }elseif ($value==5) {
     $skala=[3,5,7];
    }elseif ($value==7) {
     $skala=[5,7,9];
    }elseif ($value==9) {
     $skala=[7,9,9];
    }elseif ($value==0.33333) {
     $skala=[0.2,0.33333,1];
    }elseif ($value==0.2) {
     $skala=[0.14286,0.2,0.33333];
    }elseif ($value==0.14286) {
     $skala=[0.11111,0.14286,0.2];
    }elseif ($value==0.11111) {
     $skala=[0.11111,0.11111,0.14286];
    }
    
    
    
    return $skala;
  }
}
 
 ?>