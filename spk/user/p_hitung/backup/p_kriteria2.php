<!DOCTYPE html>

<header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/rkt2.jpg); background-position-y: center;">
    <div class=overlay-01>
    </div>
    <div class=container>
        <h2 class="text-center text-uppercase">REKOMENDASI</h2>
        <div class=breadcrumb><a href=#>Home</a> <span>/</span> 
            <a href=# class=page-active>REKOMENDASI</a>
        </div>
    </div>
</header>
<section id=contact-form class="section bg-default default-section-padding">
    <div class=container>
        <div class=row>

            <div class="col-sm-12">
                <div><center><h3>PILIH PENILAIAN BERDASARKAN</h3></center></div>

                <form action="./p_hitung/ph_kriteria2.php" class="form-horizontal" method='post' accept-charset='utf-8'>
                    <div class="panel-group" id="accordion">
                        <select class="form-control form-control-dark" name="choose"  required>
                                <option value="1">PBSI</option>
                                <option value="2">Pemilik Toko</option>
                                <option value="3">Distributor</option>
                        </select>

                        <td>
                            <input  type='submit' name='submit' class="btn btn-charity-default">
                        </td>
                        <td>
                             <a href="index.php?page=index"><button class="btn btn-default" type="button">Batal</button></a>
                        </td>
                    </div>
                </form>                           
            </div>
        </div>
    </div>
</section>
