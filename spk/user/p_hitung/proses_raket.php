<?php

include '../connect.php';
include './Metode_SAW.php';
session_start();

/* LANGKAH 1 - GET ALL KRITERIA DAN SUB KRITERIA */
/* Get all Kriteria */
$query_krit=mysqli_query($koneksidb, "SELECT * FROM kriteria");
$daftar_kriteria = [];
while ($data_krit = mysqli_fetch_array($query_krit)) {
	$daftar_kriteria[$data_krit["ID_KRITERIA"]]["data_kriteria"]["id_kriteria"] = $data_krit["ID_KRITERIA"];
	$daftar_kriteria[$data_krit["ID_KRITERIA"]]["data_kriteria"]["nama_kriteria"] = $data_krit["NAMA_KRITERIA"];
	$daftar_kriteria[$data_krit["ID_KRITERIA"]]["data_kriteria"]["bobot"] = $data_krit["BOBOT"];

	/* Get daftar sub kriteria */
	$query_sub_krit = mysqli_query($koneksidb, "SELECT * FROM subkriteria WHERE ID_KRITERIA='".$data_krit["ID_KRITERIA"]."'");

	/* Looping sub kriteria */
	$data_sub_kriteria = [];	while ($data_sub_krit = mysqli_fetch_array($query_sub_krit)) {
		/* Get data sub kriteria */
		$data_sub_kriteria[$data_sub_krit["ID_SUBKRITERIA"]]["data_sub_kriteria"]["id_subkriteria"] = $data_sub_krit["ID_SUBKRITERIA"];
		$data_sub_kriteria[$data_sub_krit["ID_SUBKRITERIA"]]["data_sub_kriteria"]["nama_subkriteria"] = $data_sub_krit["NAMA_SUBKRITERIA"];
		$data_sub_kriteria[$data_sub_krit["ID_SUBKRITERIA"]]["data_sub_kriteria"]["nilai"] = 1;
	}

	/* Jadikan satu dengan daftar kriteria */
	$daftar_kriteria[$data_krit["ID_KRITERIA"]]["daftar_sub"] = $data_sub_kriteria;
}


/* LANGKAH 2 - MENENTUKAN NILAI SUB KRITERIA */
/* Get data POST */
$data_post = $_POST["kriteria"];
foreach ($data_post as $key_krit => $value_krit) {
	/* Check apakah data dipilih */
	if (!empty($value_krit)) {
		/* Get daftar sub kriteria */
		$daftar_sub = $daftar_kriteria[$key_krit]["daftar_sub"];

		/* Looping sub Kriteria */
		$nilai = 4;
		$count_sub = count($daftar_sub);
		while(true){
			foreach ($daftar_sub as $key_sub => $value_sub) {
				/* Set nilai jika sama */
				if ($key_sub == $value_krit && $nilai == 4) {
					/* Atur nilai jadi 4 */
					$daftar_sub[$key_sub]["data_sub_kriteria"]["nilai"] = $nilai;
					$nilai--;
				} else if($key_sub != $value_krit && $nilai != 4) {
					/* Atur nilai jadi 4 */
					$daftar_sub[$key_sub]["data_sub_kriteria"]["nilai"] = $nilai;
					$nilai--;

				}
				if (($nilai == 0 && $count_sub == 4) || ($nilai == 1 && $count_sub == 3)) {
					break;
				}
			}

			$daftar_kriteria[$key_krit]["daftar_sub"] = $daftar_sub;

			if (($nilai == 0 && $count_sub == 4) || ($nilai == 1 && $count_sub == 3)) {
				break;
			}
		}
	}
}

/* LANGKAH 3 - GET DATA SEMUA RAKET */
/* Get data raket */
$daftar_raket = [];
$query_raket=mysqli_query($koneksidb, "SELECT * FROM tipe_raket");
while ($data_raket=mysqli_fetch_array($query_raket)) {
	/* Get all data from data test */
	$daftar_raket[$data_raket["ID_TIPE"]]["data_raket"]["id_tipe"] = $data_raket["ID_TIPE"];
	$daftar_raket[$data_raket["ID_TIPE"]]["data_raket"]["id_raket"] = $data_raket["ID_RAKET"];
	$daftar_raket[$data_raket["ID_TIPE"]]["data_raket"]["nama_tiperaket"] = $data_raket["NAMA_TIPERAKET"];
	$daftar_raket[$data_raket["ID_TIPE"]]["data_raket"]["gambar"] = $data_raket["GAMBAR"];

	/* Get data from data test */
	$daftar_raket_test = [];
	foreach ($daftar_kriteria as $key_krit => $value_krit) {
		$query_test = mysqli_query($koneksidb, "SELECT * FROM data_test WHERE ID_TIPE='".$data_raket["ID_TIPE"]."' AND ID_KRITERIA='".$key_krit."';");
		$data_raket_test = mysqli_fetch_array($query_test);
		$daftar_raket_test[$key_krit] = $data_raket_test["ID_SUBKRITERIA"];
	}

	/* Masukkan ke daftar raket */
	$daftar_raket[$data_raket["ID_TIPE"]]["detail_raket"] = $daftar_raket_test;
}


/* LANGKAH 4 - MASUKKAN NILAI DARI DATA RAKET */
$nilai_raket = [];
foreach ($daftar_raket as $key_raket => $value_raket) {
	/* Looping Kriteria */
	foreach ($daftar_kriteria as $key_krit => $value_krit) {
		/* Set data sub kriteria raket */
		$temp_raket = $value_raket["detail_raket"][$key_krit];

		/* Ambil nilai dari database */
		$temp_nilai = $value_krit["daftar_sub"][$temp_raket]["data_sub_kriteria"]["nilai"];

		/* Masukkan nilai dari id sub yang ada */
		$nilai_raket[$key_raket][$key_krit] = $temp_nilai;
	}
}


/* LANGKAH 5 - PROSES METODE SAW */
$SAW = new Metode_SAW($nilai_raket, $daftar_kriteria);
$hasil_hitung = $SAW->get_hasil_hitung();
// $hasil_hitung = $SAW->get_hasil_normalisasi();

echo json_encode($hasil_hitung);

/* LANGKAH 6 - MASUKKAN HASIL HITUNG KE SESSION */
$_SESSION["hasil_hitung"] = $hasil_hitung;

/* REDIRECT */
header ('Location:../index.php?page=hasilsaw');