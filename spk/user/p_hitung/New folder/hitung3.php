<!DOCTYPE html>
<html dir=ltr lang=en-US>
    <!-- Mirrored from d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 06:49:24 GMT -->
    <head>
    
    <script src="./assets/js/Jquery.min.js"></script>
    <script rel="stylesheet" href="assets/js/jQuery-2.1.4.min.js"></script>
    <script>

    $(document).ready(function(){
            $("#pilihan1").hide();
            $("#pilihan2").hide();
            $("#pilihan3").hide();
            $("#pilihan4").hide();
            $("#pilihan5").hide();
            $("#pilihan6").hide();
            $("#pilihan7").hide();
            $("#pilihan8").hide();
    });

    function ganti(pilihan){
        if(pilihan==1){
            $("#pilihan1").show();
            $("#pilihan2").hide();
            $("#pilihan3").hide();
            $("#pilihan4").hide();
            $("#pilihan5").hide();
            $("#pilihan6").hide();
            $("#pilihan7").hide();
            $("#pilihan8").hide();
        } else if(pilihan==2){
            $("#pilihan1").show();
            $("#pilihan2").show();
            $("#pilihan3").hide();
            $("#pilihan4").hide();
            $("#pilihan5").hide();
            $("#pilihan6").hide();
            $("#pilihan7").hide();
            $("#pilihan8").hide();
        } else if(pilihan==3){
            $("#pilihan1").show();
            $("#pilihan2").show();
            $("#pilihan3").show();
            $("#pilihan4").hide();
            $("#pilihan5").hide();
            $("#pilihan6").hide();
            $("#pilihan7").hide();
            $("#pilihan8").hide();
        }  else if(pilihan==4){
            $("#pilihan1").show();
            $("#pilihan2").show();
            $("#pilihan3").show();
            $("#pilihan4").show();
            $("#pilihan5").hide();
            $("#pilihan6").hide();
            $("#pilihan7").hide();
            $("#pilihan8").hide();
        }  else if(pilihan==5){
            $("#pilihan1").show();
            $("#pilihan2").show();
            $("#pilihan3").show();
            $("#pilihan4").show();
            $("#pilihan5").show();
            $("#pilihan6").hide();
            $("#pilihan7").hide();
            $("#pilihan8").hide();
        }  else if(pilihan==6){
            $("#pilihan1").show();
            $("#pilihan2").show();
            $("#pilihan3").show();
            $("#pilihan4").show();
            $("#pilihan5").show();
            $("#pilihan6").show();;
            $("#pilihan7").hide();
            $("#pilihan8").hide();
        }else if(pilihan==7){
            $("#pilihan1").show();
            $("#pilihan2").show();
            $("#pilihan3").show();
            $("#pilihan4").show();
            $("#pilihan5").show();
            $("#pilihan6").show();
            $("#pilihan7").show();
            $("#pilihan8").hide();
        }else if(pilihan==8){
            $("#pilihan1").show();
            $("#pilihan2").show();
            $("#pilihan3").show();
            $("#pilihan4").show();
            $("#pilihan5").show();
            $("#pilihan6").show();
            $("#pilihan7").show();
            $("#pilihan8").show();
        }else {
            $("#pilihan1").hide();
            $("#pilihan2").hide();
            $("#pilihan3").hide();
            $("#pilihan4").hide();
            $("#pilihan5").hide();
            $("#pilihan6").hide();
            $("#pilihan7").hide();
            $("#pilihan8").hide();
        }
    }
    </script>
    </head>

    <body>
    <div id="wrapper">
        <div id="loader">
            
        </div>
    </div>
    <div class="main_container">
            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/m1.jpg); background-position-y: bottom;"><div class=overlay-01></div><div class=container><h2 class="text-center text-uppercase">PERHITUNGAN</h2><div class=breadcrumb><a href=#>Home</a> <span>/</span> <a href=# class=page-active>PERHITUNGAN</a></div></div></header>
            <section id=contact-form class="section bg-default default-section-padding">
                <div class=container>
                    <div class=row>
                        <div class="col-sm-5 col-md-offset-3 col-md-6">
                            <div><center><h3>PENGISIAN DATA</h3></center></div>
                            <h4>
                            </h4>
                            <form action="./p_hitung/ph_hitung1.php"  method='post' accept-charset='utf-8'>
                                <div class="form-group">
                                    <label>Banyak Pilihan</label>
                                    <select name="pilihan" class="form-control" onchange="ganti(this.value)">
                                        <option selected disabled value="">--Banyaknya Pilihan Anda--</option>
                                        <?php
                                        for ($index = 1; $index < 9; $index++) {
                                        ?>
                                            <option value="<?php echo $index ?>"><?php echo $index ?> Pilihan</option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group" id="banyak_pilihan">
                                    <?php for ($index = 1; $index < 9; $index++) {
                                        ?>
                                        <div id="pilihan<?php echo $index ?>">
                                            <label  class='dark-text'>
                                                <p style="font-weight: bold">Pilihan <?php echo $index ?></p>
                                            </label>
                                            <div class=row>
                                                <div class="col-md-6">
                                                    <?php
                                                    include './connect.php';
                                                    $qi = mysql_query("SELECT * from kriteria");
                                                    ?>
                                                    <select name="kriteria<?php echo $index?>" id="kr<?php echo $index ?>" class="form-control" onchange="sub(<?php echo $index ?>)">
                                                        <option selected disabled value="">--Pilihan Kriteria--</option>
                                                        <?php while ($iqi = mysql_fetch_array($qi)) { ?>
                                                            <option value='<?php echo $iqi['ID_KRITERIA']?>'><?php echo $iqi['NAMA_KRITERIA'] ?></option>
                                                        <?php }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <select name="sub_kr<?php echo $index ?>" id="sub_kr<?php echo $index ?>" class="form-control">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    <?php }
                                    ?>

                                </div>
                                <div class='col-sm-12'>
                                    <input id='cfsubmit' type='submit'  class="btn btn-charity-default">
                                </div>
                                <div id='contactFormResponse'>
                                </div>
                                <input type="hidden" name="number" value="Norway">
                            </form>                           
                        </div>
                    </div>
                </div>
            </section>
        <script>
                    function sub(id) {       
                        xhttp = new XMLHttpRequest();
                        xhttp.onreadystatechange = function() {
                            if (xhttp.readyState == 4 && xhttp.status == 200) {
                                var a = "sub_kr" + id;
                            document.getElementById(a).innerHTML = xhttp.responseText;
                            }
                        };
                            var b = "kr" + id;
                        id_perencanaan=document.getElementById(b).value;
                        xhttp.open("GET", "http://localhost/Projectx/user/p_hitung/ajax_subkriteria.php?q=" + id_perencanaan, true);
                        xhttp.send();
                    }
        </script>

    </body>

    <!-- Mirrored from d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 06:49:26 GMT -->
</html>
