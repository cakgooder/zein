<!DOCTYPE html>

<header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/m1.jpg); background-position-y: bottom;">
    <div class=overlay-01>
    </div>
    <div class=container>
        <h2 class="text-center text-uppercase">PERHITUNGAN</h2>
        <div class=breadcrumb>
            <a href=#>Home</a> <span>/</span> 
            <a href=# class=page-active>PERHITUNGAN</a>
        </div>
    </div>
</header>
<section id=contact-form class="section bg-default default-section-padding">
    <div class=container>
        <div class=row>
            <h4><i class="fa fa-angle-right"></i><b> Petunjuk Pengisian </b></h4>
            <div class="alert alert-warning"> 
                <p>Jika Subkriteria sebelah kiri atas memiliki nilai sama penting dengan kriteria dibawahnya maka pilih sama penting.</p>
                <p>Jika Subkriteria sebelah kiri atas memiliki nilai sedikit lebih penting dengan kriteria dibawahnya maka pilih sedikit lebih penting.</p>
                <p>Jika Subkriteria sebelah kiri atas memiliki nilai lebih penting daripada kriteria dibawahnya maka pilih lebih penting.</p>
                <p>Jika Subkriteria sebelah kiri atas memiliki nilai sangat lebih penting daripada kriteria dibawahnya maka pilih sangat penting.</p>
                <p>Jika Subkriteria sebelah kiri atas memiliki nilai mutlak sangat penting daripada kriteria bawahnya maka isikan mutlak sangat penting.</p>
                <p>Contoh: apabila sub bahan frame <b>Graphite</b> memiliki nilai sedikit lebih penting daripada sub bahan frame <b>Carbon</b> maka pilih sedikit lebih penting.</p>
            </div> 
            <div class="col-sm-12">
                <div><center><h3>PENGISIAN DATA</h3></center></div>
                <form action="./p_hitung/ph_subkriteria.php" class="form-horizontal"  method='post' accept-charset='utf-8'>
                    <div class="panel-group" id="accordion">
                        <?php
                        include 'connect.php';
                        $kriteria = [];
                        $i = 0;
                        $q1 = mysql_query("SELECT * from kriteria");
                        while ($iq1 = mysql_fetch_array($q1)) {
                            $kriteria[$i]['id'] = $iq1['ID_KRITERIA'];
                            $kriteria[$i]['nama'] = $iq1['NAMA_KRITERIA'];
                            $i++;
                        }
                        $zzz = 0;
                        ?>
                        <?php foreach ($kriteria as $value): ?>
                            <?php
                            $zzz++;
                            $pembanding = $value['nama'];
                            ?>
                            <input type="hidden" name="kriteria[]" value="<?php echo $value['id'] ?>">
                            <!-- <h2><?php echo $value['nama'] ?></h2>  -->
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $zzz; ?>">
                                            <?php echo $pembanding ?>
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapse<?php echo $zzz; ?>" class="panel-collapse collapse">
                                      <div class="panel-body">
                                          <div class='col-md-12'>
                                            <div class='form-group'>
                                                <?php
                                                $subkriteria = [];
                                                $k = 0;
                                                $sq2 = mysql_query("SELECT * from subkriteria where id_kriteria='$value[id]'");
                                                while ($isq2 = mysql_fetch_array($sq2)) {
                                                    $subkriteria[$k]['id'] = $isq2["ID_SUBKRITERIA"];
                                                    $subkriteria[$k]['nama'] = $isq2["NAMA_SUBKRITERIA"];
                                                    $k++;
                                                    ?>
                                                    <input type="hidden" name="<?php echo $value['id'] ?>[]" value="<?php echo $isq2['ID_SUBKRITERIA'] ?>">

                                                    <?php
                                                }
                                                $i = 0;
                                                foreach ($subkriteria as $value2):
                                                    $banding = $value2['nama'];
                                                    ?>
                                                    <!-- <h4><?php echo $value2['nama'] ?>dd</h4> -->
                                                    <br>
                                                    <div class='col-md-12'>
                                                        <div class='form-group'>
                                                            <?php
                                                            $j = 0;
                                                            foreach ($subkriteria as $value3):
                                                                ?>

                                                                <?php if ($value2['id'] == $value3['id']): ?>
                                                                    <input type="hidden" name="<?php echo $value['id'] ?>subkriteria[<?php echo $value2['id'] ?>][<?php echo $value3['id'] ?>]" value="1">
                                                                <?php else: ?>
                                                                    <?php if ($j > $i): ?>
                                                                        <div class="form-group">
                                                                            <label for='InputPhoneNumber' class='dark-text col-md-4' align="right"><?php echo $banding ?></label> 
                                                                            <div class="col-md-4">
                                                                                <select class="form-control form-control-dark"name="<?php echo $value['id'] ?>subkriteria[<?php echo $value2['id'] ?>][<?php echo $value3['id'] ?>]" id='InputName'  required>
                                                                                    <option value="1">Sama Penting</option>
                                                                                    <option value="3">Sedikit Lebih Penting</option>
                                                                                    <option value="5">Lebih Penting</option>
                                                                                    <option value="7">Sangat Penting</option>
                                                                                    <option value="9"> Mutlak Sangat Penting</option>
                                                                                    <option value="0.33333333">Kurang Penting</option>
                                                                                    <option value="0.2"> Sedikit Tidak Penting</option>
                                                                                    <option value="0.14285714">Tidak Penting</option>
                                                                                    <option value="0.11111111">Mutlak Tidak Penting</option>                                                   
                                                                                </select>
                                                                            </div>
                                                                            <label for='InputPhoneNumber' class='dark-text col-md-4'><?php echo $value3['nama'] ?></label> 
                                                                        </div>
                                                                        <!-- <br> -->
                                                                    <?php else: ?>
                                                                        <input type="hidden" name="<?php echo $value['id'] ?>subkriteria[<?php echo $value2['id'] ?>][<?php echo $value3['id'] ?>]" value="0">
                                                                    <?php endif ?>

                                                                <?php endif ?>
                                                                <?php $j++; ?>   
                                                            <?php endforeach ?>
                                                            <hr>
                                                        </div>
                                                    </div>
                                                    <?php $i++; ?>
                                                <?php endforeach ?>
                                            </div>
                                          </div>
                                      </div>
                                    </div>   
                                </div>
                            </div>
                            <br>      
                        <?php endforeach ?>
                        <div class='col-sm-12'>
                            <tr>
                                <td>
                                    <input  type='submit' name='submit' class="btn btn-charity-default">
                                </td>
                                <td>
                                    <a href="index.php?page=index"><button class="btn btn-default" type="button">Batal</button></a>
                                </td>
                            </tr>
                        </div>
                        <div id='contactFormResponse'>
                        </div>
                    </div>
                </form>                           
            </div>
        </div>
    </div>
</section>