<!DOCTYPE html>
<html dir=ltr lang=en-US>
    <!-- Mirrored from d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 06:49:24 GMT -->
    <head><meta charset=UTF-8><meta http-equiv=X-UA-Compatible content="IE=edge"><meta name=viewport content="width=device-width, initial-scale=1"><meta name=author content=ThemeWagon><meta name=Description content="Rights Non-Profit Template"><meta name=keywords content="non-profit, child"><title>Perhitungan | Rights</title><link rel=apple-touch-icon sizes=57x57 href=assets/images/favicons/apple-icon-57x57.png><link rel=apple-touch-icon sizes=60x60 href=assets/images/favicons/apple-icon-60x60.png><link rel=apple-touch-icon sizes=72x72 href=assets/images/favicons/apple-icon-72x72.png><link rel=apple-touch-icon sizes=76x76 href=assets/images/favicons/apple-icon-76x76.png><link rel=apple-touch-icon sizes=114x114 href=assets/images/favicons/apple-icon-114x114.png><link rel=apple-touch-icon sizes=120x120 href=assets/images/favicons/apple-icon-120x120.png><link rel=apple-touch-icon sizes=144x144 href=assets/images/favicons/apple-icon-144x144.png><link rel=apple-touch-icon sizes=152x152 href=assets/images/favicons/apple-icon-152x152.png><link rel=apple-touch-icon sizes=180x180 href=assets/images/favicons/apple-icon-180x180.png><link rel=icon type=image/png sizes=192x192 href=assets/images/favicons/android-icon-192x192.png><link rel=icon type=image/png sizes=32x32 href=assets/images/favicons/favicon-32x32.png><link rel=icon type=image/png sizes=96x96 href=assets/images/favicons/favicon-96x96.png><link rel=icon type=image/png sizes=16x16 href=assets/images/favicons/favicon-16x16.png><link rel=manifest href=assets/images/favicons/manifest.json><meta name=msapplication-TileColor content=#ffffff><meta name=msapplication-TileImage content=assets/images/favicons/ms-icon-144x144.png><meta name=theme-color content=#ffffff><link rel=stylesheet href=assets/lib/bootstrap/dist/css/bootstrap.min.css><link rel=stylesheet href=assets/lib/owlcarousel/owl-carousel/owl.carousel.css><link rel=stylesheet href=assets/lib/owlcarousel/owl-carousel/owl.theme.css><link rel=stylesheet href=assets/lib/ionicons/css/ionicons.css><link rel=stylesheet href=assets/lib/fontawesome/css/font-awesome.min.css><link rel=stylesheet href=assets/extras/swipebox/css/swipebox.min.css><link rel=stylesheet href=assets/extras/rotating-carousel/css/style.css><link rel=stylesheet href=assets/extras/slick/slick.css><link rel=stylesheet href=assets/extras/magnificpopup/magnific-popup.css><link rel=stylesheet href=assets/css/main.css><link rel=stylesheet href=assets/css/custom.css></head><body><div id=wrapper><div id=loader></div></div><div class=main_container>
            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/m1.jpg); background-position-y: bottom;"><div class=overlay-01></div><div class=container><h2 class="text-center text-uppercase">PERHITUNGAN</h2><div class=breadcrumb><a href=#>Home</a> <span>/</span> <a href=# class=page-active>PERHITUNGAN</a></div></div></header>
            <section id=contact-form class="section bg-default default-section-padding">
                <div class=container>
                    <div class=row>
                        <div class="col-sm-5 col-md-offset-3 col-md-6">
                            <div><center><h3>PENGISIAN DATA</h3></center></div>
                            <form action="#"  method='post' accept-charset='utf-8'>
                                <div class="form-group">
                                    <label>NAMA KRITERIA</label>
                                    <select name="jenis" name="jumlah" class="form-control">
                                        <option selected disabled value="">--Banyaknya Pilihan Anda--</option>
                                        <?php
                                        for ($index = 1; $index < 9; $index++) {
                                            ?><option value="<?php echo $index ?>"><?php echo $index ?> Pilihan</option><?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div id="nama">Harunya disini</div>
                                </div>
                                <div class='col-sm-12'>
                                    <input id='cfsubmit' type='submit'  class="btn btn-charity-default">
                                </div>
                                <div id='contactFormResponse'>
                                </div>
                            </form>                           
                        </div>
                    </div>
                </div>
            </section>

            <!--script dropdown tingkat-->
            <script>
                $(function () { // sama dengan $(document).ready(function(){

                    $('#jenis').change(function () {

                        $('#nama').html("<img src='<?php echo base_url() ?>asset/js/ajax-loader.gif'>"); //Menampilkan loading selama proses pengambilan data kota

                        var id = $(this).val(); //Mengambil id provinsi

                        $.get("./select.php", {id: id}, function (data) {
                            $('#nama').html(data);
                        });
                    });

                });
            </script>
            <script type="text/javascript" src="./assets/js/jQuery-2.1.4.min.js"></script>
            
            
    </body>
    <!-- Mirrored from d2zav2bjdlctd5.cloudfront.net/theme_preview/rights/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 May 2016 06:49:26 GMT -->
</html>