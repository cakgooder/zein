<!DOCTYPE html>
    <?php @session_start(); ?>

            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/rkt2.jpg); background-position-y: center;">
                <div class=overlay-01></div>
                <div class=container><h2 class="text-center text-uppercase">REKOMENDASI</h2>
                    <div class=breadcrumb>
                        <a href=#>Home</a> <span>/</span> 
                        <a href=# class=page-active>REKOMENDASI</a>
                    </div>
                </div>
            </header>
            <section id=contact-form class="section bg-default default-section-padding">
                <div class=container>
                    <h3 style="text-align:center; margin: 30px">HASIL REKOMENDASI</h3>
                    
                    <div class=row>
                        <div class="col-sm-12">
                            <form class="form-horizontal" action="./p_hitung/simpanhasil.php" method="post">
                                <div class="form-group">
                                    <label class="col-md-3" align="right">Nama</label>
                                    <div class="col-md-6"><input type="text" name="nama" class="col-md-6"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3" align="right"></label>
                                    <div class="col-md-1"><button type="submit" class="button green">Simpan</button></div>
                                </div>
                            </form>
                        <div class="col-sm-12">
                    </div>
                    <div class=row>
                        <div class="col-sm-12">
                            <?php
                            include 'connect.php';
                            $hasil_hitung = $_SESSION['hasil_hitung'];
                            ?>
                            <table border="2px" style="width:960px;" align="center">
                                <tr style="font-weight:bold">
                                    <th style="text-align:center">No.</th>
                                    <th style="text-align:center">NAMA PRODUSEN</th>
                                    <th style="text-align:center">NAMA TIPE RAKET</th>
                                    <th style="text-align:center">NILAI</th>
                                </tr>
                                <?php
                                $index = 1;
                                foreach ($hasil_hitung as $key => $value) {
                                    ?>
                                    <tr>
                                        <td style="text-align:center">
                                            <?php echo $index; ?>
                                        </td>
                                         <td style="text-align:center">
                                            <?php
                                            $sq = mysqli_query($koneksidb, "SELECT * from tipe_raket a, produsen_raket b where a.id_tipe='$key' and a.id_raket=b.id_raket ");

                                            while ($isq = mysqli_fetch_array($sq)) {
                                                echo ucfirst(strtolower($isq['NAMA_PRODUSENRAKET']));
                                                echo "<br>";
                                            }
                                            ?>
                                        </td>
                                         <td style="text-align:center">
                                            <?php
                                            $sq = mysqli_query($koneksidb, "SELECT * from tipe_raket a, produsen_raket b where a.id_tipe='$key' and a.id_raket=b.id_raket ");

                                            while ($isq = mysqli_fetch_array($sq)) {
                                                ?>
                                                <a href="index.php?page=detail_galeri&gambar=<?php echo $key ?>" target="_blank"><?php echo $isq['NAMA_TIPERAKET'];?></a>
                                                <?php
                                                
                                                echo "<br>";
                                            }
                                            ?>
                                        </td>
                                         <td style="text-align:center">
                                            <?php
                                            echo $value;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $index++;

                                    if ($index == 4) {
                                        break;
                                    }
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        