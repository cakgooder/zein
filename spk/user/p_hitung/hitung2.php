<!DOCTYPE html>

            <header class="inner-header overlay grey text-center slim-bg" style="background-image: url(assets/images/headers/rkt2.jpg); background-position-y: center;">
                <div class="overlay-01">

                </div>
                <div class="container">
                    <h2 class="text-center text-uppercase">REKOMENDASI</h2>
                    <div class="breadcrumb">
                        <a href="#">Home</a> <span>/</span> 
                        <a href="#" class="page-active">REKOMENDASI</a>
                    </div>
                </div>
            </header>
            <section id="contact-form" class="section bg-default default-section-padding">
                <div class="container">
                    <div class="row">
                        <div class="alert alert-warning">
                            <p align="center">silahkan isi kriteria raket yang diinginkan.</p>
                        </div>  
                        <div class="col-sm-12">
                            <div>
                                <center><h3>PENGISIAN DATA</h3></center>
                            </div>
                            <form action="./p_hitung/proses_raket.php"  method="post" accept-charset="utf-8">
                                <?php
                                session_destroy();
                                include './connect.php';
                                $qi = mysqli_query($koneksidb, "SELECT * from kriteria");
                                while ($iqi = mysqli_fetch_array($qi)) {
                                    ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label  class="dark-text"><?php echo $iqi['NAMA_KRITERIA'] ?></label> 
                                            <select class="form-control form-control-dark" name="kriteria[<?php echo $iqi['ID_KRITERIA'] ?>]"  required>
                                                <?php
                                                $q2 = mysqli_query($koneksidb, "SELECT * from subkriteria where id_kriteria='$iqi[ID_KRITERIA]'");
                                                ?>
                                                <option value="0"></option>
                                                <?php
                                                while ($iq2 = mysqli_fetch_array($q2)) {
                                                    ?>
                                                    <option value="<?php echo $iq2['ID_SUBKRITERIA'] ?>"><?php echo $iq2['NAMA_SUBKRITERIA'] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="col-sm-12">
                                    <tr>
                                        <td>
                                        <input id="cfsubmit" type="submit"  class="btn btn-charity-default">
                                        </td>
                                        <td>
                                             <a href="index.php?page=index"><button class="btn btn-default" type="button">Batal</button></a>
                                        </td>
                                    </tr>
                                </div>
                                <div id="contactFormResponse">
                                </div>
                            </form>                           
                        </div>
                    </div>
                </div>
            </section>
       