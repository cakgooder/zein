<?php

/**
 * aIProject
 */
class SawAssosiatif {

    private $matrixPerbandingan = [];
    private $jumPerKriteria = [];
    private $prioritas = [];
    private $perKriteria = [];
    private $ukuran = 0;
    private $lamdaMax = 0;
    private $CI = 0;
    private $CR = 0;
    private $kolom = [];

    function __construct() {
        
    }

    public function setMatrix($matrix=[],$kolom=[]) {
        $this->setUkuran($matrix);
        $this->setKolom($kolom);
        $this->setMatrixPerbandingan($matrix);
         $this->setJumPerKriteria();
        $this->setPrioritas();
         $this->setPerKriteria();
        $this->konsistensi();
       
    }

    public function setKolom($kolom)
    {
        $this->kolom=$kolom;
    }

    public function getKolom()
    {
        return $this->kolom;
    }

    public function setUkuran($matrix) {
        $this->ukuran = count($matrix);
    }

    public function getUkuran() {
        return $this->ukuran;
    }

    public function setMatrixPerbandingan($matrix) {
        $i=0;
        foreach ($this->getKolom() as $key) {
            $j=0;
            foreach ($this->getKolom() as $key2) {
                if ($key == $key2) {
                    $matrix[$key][$key2] = 1;
                }else{
                    if($j>$i){
                        $matrix[$key2][$key] = round(1 / $matrix[$key][$key2],5);
                    }
                }
                $j++;
            }
            $i++;
        }
       $this->matrixPerbandingan = $matrix;
    }

    public function getMatrixPerbandingan() {
        return $this->matrixPerbandingan;
    }

    public function setJumPerKriteria() {

        $this->jumPerKriteria = $this->sumPerKolom($this->getMatrixPerbandingan(),$this->getKolom());
    }

    public function getJumPerKriteria() {

        return $this->jumPerKriteria;
    }

    public function setPrioritas() {
        $temp = [];
        $jumPerKriteria = $this->getJumPerKriteria();
        $matrixPerbandingan = $this->getMatrixPerbandingan();
        foreach ($this->getKolom() as $key) {
            foreach ($this->getKolom() as $key2) {
                $temp[$key][$key2] = $matrixPerbandingan[$key][$key2] / $jumPerKriteria[$key2];
            }
        }
        $temp = $this->sumPerBaris($temp,$this->getKolom());

        foreach ($this->getKolom() as $key) {
            $temp[$key] = $temp[$key] / $this->getUkuran();
        }
        $this->prioritas = $temp;
    }

    public function getPrioritas() {
        return $this->prioritas;
    }

    public function setPerKriteria() {
        $matrixPerbandingan = $this->getMatrixPerbandingan();
        $prioritas = $this->getPrioritas();
        $temp = [];

        foreach ($this->getKolom() as $key) {
            $temp2 = 0;
            foreach ($this->getKolom() as $key2) {
                $temp2+=$matrixPerbandingan[$key][$key2] * $prioritas[$key2];
            }
            $temp[$key] = $temp2;
        }
        $this->perKriteria = $temp;
    }

    public function getPerKriteria() {
        return $this->perKriteria;
    }

    public function konsistensi() {
        $perKriteria = $this->getPerKriteria();
        $prioritas = $this->getPrioritas();
        $temp = 0;
        foreach ($this->getKolom() as $key) {
            $temp+=$perKriteria[$key] + $prioritas[$key];
        }
        $this->lamdaMax = $temp / $this->getUkuran();
        $this->CI = ($this->lamdaMax - $this->getUkuran()) / ($this->getUkuran() - 1);
        $ir=$this->IR($this->getUkuran());
        if($ir==0){
            $this->CR =0;
        }else{
            $this->CR = $this->CI / $this->IR($this->getUkuran());

        }
    }

    public function getLamdaMax() {
        return $this->lamdaMax;
    }

    public function getCI() {
        return $this->CI;
    }

    public function getCR() {
        return $this->CR;
    }

    public function cekKonsistensi() {
        $cek = false;
        if ($this->getCR() <= 0.1) {
            $cek = true;
        }
        return $cek;
    }

    public function IR($ukuran) {
        $IR = array(0.0, 0.0, 0.58, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49, 1.51, 1.48, 1.56, 1.57, 1.59);
        return $IR[$ukuran - 1];
    }

    public function sumPerBaris($matrix,$kolom) {
        $jumPerBaris = [];

        foreach ($kolom as $key ) {
            $jumPerBaris[$key] = array_sum($matrix[$key]);
        }

        return $jumPerBaris;
    }

    public function sumPerKolom($matrix,$kolom=[]) {
       
        $jumPerKolom = [];
        foreach ($kolom as $key ) {
            $temp = 0;
            foreach ($kolom as $key2) {
               $temp+=$matrix[$key2][$key];

            }
            
            $jumPerKolom[$key] = $temp;
        }
        

        return $jumPerKolom;
    }

}
?>