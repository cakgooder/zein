<?php
class Metode_SAW {

	private $rangking = 0;
	private $nilai_normalisasi = 0;
	private $nilai_max = 0;
	private $hasil_hitung = 0;

	public function __construct($nilai_raket, $daftar_kriteria, $jenis_bobot = 1)
	{
		//Do your magic here
		/* STEP METODE SAW */

		/* STEP 1 */
		/* Mencari nilai max */
		$this->nilai_max = $this->cari_nilai_max($nilai_raket, $daftar_kriteria);

		/* STEP 2 */
		/* Normalisasi */
		$this->nilai_normalisasi = $this->proses_normalisasi($nilai_raket, $daftar_kriteria, $this->nilai_max);

		/* STEP 3 */
		/* Mendapatkan bobot per kriteria */
		$bobot_kriteria = $this->get_bobot_kriteria($daftar_kriteria, $jenis_bobot);

		/* STEP 4 */
		/* Penghitungan */
		$this->hasil_hitung = $this->proses_hitung_saw($daftar_kriteria, $this->nilai_normalisasi, $bobot_kriteria);

		/* STEP 5 */
		/* Perangkingan */
		$this->rangking = $this->proses_rangking($this->hasil_hitung);
	}

	/* Fungsi untuk cari nilai max masing2 kriteria */
	public function cari_nilai_max($nilai_raket, $daftar_kriteria)
	{
		$max = [];
		/* Looping per kriteria */
		foreach ($daftar_kriteria as $key_krit => $value_krit) {
			$max[$key_krit] = 1;

			/* Looping per raket */
			foreach ($nilai_raket as $key_nilai => $value_nilai) {
				if ($max[$key_krit] < $value_nilai[$key_krit]) {
					$max[$key_krit] = $value_nilai[$key_krit];
				}
			}
		}

		return $max;
	}

	/* Fungsi nilai normalisasi */
	public function proses_normalisasi($nilai_raket, $daftar_kriteria, $nilai_max)
	{
		$normalisasi = [];
		/* Looping per raket */
		foreach ($nilai_raket as $key_raket => $value_raket) {
			/* Looping per kriteria */
			foreach ($daftar_kriteria as $key_krit => $value_krit) {
				$normalisasi[$key_raket][$key_krit] = (double) $value_raket[$key_krit] / $nilai_max[$key_krit];
			}
		}

		return $normalisasi;
	}

	/* Get bobot masing2 kriteria */
	public function get_bobot_kriteria($daftar_kriteria, $jenis_bobot = 1)
	{
		$bobot = [];
		/* Looping per kriteria untuk mengambil bobot */
		foreach ($daftar_kriteria as $key_krit => $value_krit) {
			if ($jenis_bobot == 1) {
				$bobot[$key_krit] = $value_krit["data_kriteria"]["bobot"];
			}
			/* Else Bobot by */
			// else if($jenis_bobot == 2){
			// 	$bobot[$key_krit] = $value_krit["data_kriteria"]["bobot_pbsi"];
			// }
		}

		return $bobot;
	}

	/* Fungsi Proses Perhitungan SAW */
	public function proses_hitung_saw($daftar_kriteria, $nilai_normalisasi, $bobot)
	{
		$hasil_hitung = [];
		/* Looping by per raket */
		foreach ($nilai_normalisasi as $key_raket => $value_raket) {
			$temp_sigma = 0;
			foreach ($daftar_kriteria as $key_krit => $value_krit) {
				/* Menjumlahkan hasil perhitungan */
				$temp_sigma += (double) ($value_raket[$key_krit] * $bobot[$key_krit]) / 100;
			}

			/* Masukkan ke hasil hitung */
			$hasil_hitung[$key_raket] = round($temp_sigma, 2);
		}

		return $hasil_hitung;
	}

	/* Fungsi proses rangking */
	public function proses_rangking($hasil_hitung, $jenis_sort = true)
	{
		/* Pengecekan jenis sort */
		if ($jenis_sort) {
			/* DESCENDING */
			arsort($hasil_hitung);
			return $hasil_hitung;
		} else {
			/* ASCENDING */
			asort($hasil_hitung);
			return $hasil_hitung;
		}
	}

	/* Fungsi GET */
	/* GET rangking */
	public function get_hasil_hitung()
	{
		return $this->rangking;
	}

	/* GET hasil normalisasi */
	public function get_hasil_normalisasi()
	{
		return $this->nilai_normalisasi;
	}

	/* GET hasil max */
	public function get_nilai_max()
	{
		return $this->nilai_max;
	}

}

/* End of file Metode_SAW.php */
/* Location: ./var/www/zein/spk/user/p_hitung/Metode_SAW.php */