<?php
	//Gunakan Koneksi
	include("koneksi.php");
	//Buat array bobot { C1 = 35%; C2 = 25%; C3 = 25%; dan C4 = 15%.}
	$bobot = array(0.35, 0.25, 0.25, 0.15);

	//Buat fungsi tampilkan nama
	function getNama($id){
		$q =mysqli_query($koneksidb, "SELECT * FROm tbcalon where idCalon = '$id'");
		$d = mysqli_fetch_array($q);
		return $d['nama'];
	}
	
	//Setelah bobot terbuat select semua di tabel Matrik
	$sql = mysqli_query($koneksidb, "SELECT * FROM tbmatrik");
	//Buat tabel untuk menampilkan hasil
	echo "<H3>Matrik Awal</H3>
	<table width=500 style='border:1px; #ddd; solid; border-collapse:collapse' border=1>
		<tr>
			<td>No</td><td>Nama</td><td>C1</td><td>C2</td><td>C3</td><td>C4</td>
		</tr>
		";
	$no = 1;
	while ($dt = mysqli_fetch_array($sql)) {
		echo "<tr>
			<td>$no</td><td>".getNama($dt['idCalon'])."</td><td>$dt[Kriteria1]</td><td>$dt[Kriteria2]</td><td>$dt[Kriteria3]</td><td>$dt[Kriteria4]</td>
		</tr>";
	$no++;
	}
	echo "</table>";

	//Lakukan Normalisasi dengan rumus pada langkah 2
	//Cari Max atau min dari tiap kolom Matrik
	$crMax = mysqli_query($koneksidb, "SELECT max(Kriteria1) as maxK1, 
						max(Kriteria2) as maxK2,
						max(Kriteria3) as maxK3,
						max(Kriteria4) as maxK4 
			FROM tbmatrik");
	$max = mysqli_fetch_array($crMax);

	//Hitung Normalisasi tiap Elemen
	$sql2 = mysqli_query($koneksidb, "SELECT * FROM tbmatrik");
	//Buat tabel untuk menampilkan hasil
	echo "<H3>Matrik Normalisasi</H3>
	<table width=500 style='border:1px; #ddd; solid; border-collapse:collapse' border=1>
		<tr>
			<td>No</td><td>Nama</td><td>C1</td><td>C2</td><td>C3</td><td>C4</td>
		</tr>
		";
	$no = 1;
	while ($dt2 = mysqli_fetch_array($sql2)) {
		echo "<tr>
			<td>$no</td><td>".getNama($dt2['idCalon'])."</td><td>".round($dt2['Kriteria1']/$max['maxK1'],2)."</td><td>".round($dt2['Kriteria2']/$max['maxK2'],2)."</td><td>".round($dt2['Kriteria3']/$max['maxK3'],2)."</td><td>".round($dt2['Kriteria4']/$max['maxK4'],2)."</td>
		</tr>";
	$no++;
	}
	echo "</table>";

	//Proses perangkingan dengan rumus langkah 3
	$sql3 = mysqli_query($koneksidb, "SELECT * FROM tbmatrik");
	//Buat tabel untuk menampilkan hasil
	echo "<H3>Perangkingan</H3>
	<table width=500 style='border:1px; #ddd; solid; border-collapse:collapse' border=1>
		<tr>
			<td>No</td><td>Nama</td><td>Rangking</td>
		</tr>
		";
	$no = 1;
	//Kita gunakan rumus (Normalisasi x bobot)
	while ($dt3 = mysqli_fetch_array($sql3)) {
		echo "<tr>
			<td>$no</td><td>".getNama($dt3['idCalon'])."</td>
			<td>"
			.round((($dt3['Kriteria1']/$max['maxK1'])*$bobot[0])+
			(($dt3['Kriteria2']/$max['maxK2'])*$bobot[1])+
			(($dt3['Kriteria3']/$max['maxK3'])*$bobot[2])+
			(($dt3['Kriteria4']/$max['maxK4'])*$bobot[3]),2)."</td>
		</tr>";
	$no++;
	}
	echo "</table>";

?>