-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 30. Juni 2013 jam 01:02
-- Versi Server: 5.5.16
-- Versi PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbcalon`
--

CREATE TABLE IF NOT EXISTS `tbcalon` (
  `idCalon` int(3) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  PRIMARY KEY (`idCalon`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `tbcalon`
--

INSERT INTO `tbcalon` (`idCalon`, `nama`) VALUES
(1, 'Indra'),
(2, 'Roni'),
(3, 'Putri'),
(4, 'Dani'),
(5, 'Ratna'),
(6, 'Mira');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbmatrik`
--

CREATE TABLE IF NOT EXISTS `tbmatrik` (
  `idMatrik` int(3) NOT NULL AUTO_INCREMENT,
  `idCalon` int(3) NOT NULL,
  `Kriteria1` double NOT NULL,
  `Kriteria2` double NOT NULL,
  `Kriteria3` double NOT NULL,
  `Kriteria4` double NOT NULL,
  PRIMARY KEY (`idMatrik`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data untuk tabel `tbmatrik`
--

INSERT INTO `tbmatrik` (`idMatrik`, `idCalon`, `Kriteria1`, `Kriteria2`, `Kriteria3`, `Kriteria4`) VALUES
(1, 1, 70, 50, 80, 60),
(2, 2, 50, 60, 82, 70),
(3, 3, 85, 55, 80, 75),
(4, 4, 82, 70, 65, 85),
(5, 5, 75, 75, 85, 74),
(6, 6, 62, 50, 75, 80);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
